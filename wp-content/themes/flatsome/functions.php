<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */


// Let's roll!
//extant_theme();
function spotire_styles()
{
    wp_enqueue_script('scripjscustom', get_template_directory_uri() . '/assets/js/hangduc.js', array('jquery'));
}

add_action('wp_enqueue_scripts', 'spotire_styles');