jQuery(document).ready(function () {
    jQuery(".resent_product .products").slick({
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 5,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 5
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 2
            }
        }]
    });

    jQuery('.banner_mini .products').slick({
        rows: 2,
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 5
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 2
            }
        }]
    });

    jQuery(".flash_sales_countdown .products").slick({
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 5,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 5
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 2
            }
        }]
    });

    jQuery('.slider_uudai_tt .section-content').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    jQuery('.slider_uudai_th .section-content').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 1
            }
        }]
    });


    function alignHeight(selector) {
        setTimeout(function () {
            jQuery(selector).css('height', '');
            var minHeight = 0;
            jQuery(selector).each(function () {
                if (minHeight < jQuery(this).outerHeight()) {
                    minHeight = jQuery(this).outerHeight();
                }
            });
            if (minHeight > 0) {
                jQuery(selector).css('height', minHeight);
            }
        }, 500)
    }

    jQuery(window).on('load resize', function () {
        if(jQuery('div').hasClass('box-text-products')) {
            alignHeight('.box-text-products');
            alignHeight('.flash_sales_countdown .product .col-inner');
        }
        if (jQuery(window).width() > 767) {
            alignHeight('.postid-662 .elementor-widget-container h3');
        }
    });
});





