<?php namespace TierPricingTable\Integrations\Plugins;

class WCPA extends PluginIntegrationAbstract {

	public function addAddonsPriceToItem( $price, $cart_item ) {

		if ( $price && ! empty( $cart_item['wcpa_options_price_start'] ) ) {
			$price += $cart_item['wcpa_options_price_start'];
		}

		return $price;
	}

	public function getTitle() {
		return __( 'WooCommerce Custom Product Addons (WCPA)', 'tier-pricing-table' );
	}

	public function getDescription() {
		return __( 'Integration provides compatibility with WCPA to work properly with custom product options.',
			'tier-pricing-table' );
	}

	public function getSlug() {
		return 'wcpa';
	}

	public function isPluginActive() {
		return true;
	}

	/**
	 * Render compatibility script
	 */
	public function addCompatibilityScript() {

		if ( ! is_product() ) {
			return;
		}

		?>
		<script>
            /**
             * Tiered Pricing WOOCS Compatibility
             */
            (function ($) {
                $(document).on('tiered_price_update', function (event, data) {
                    $.each($('.wcpa_form_outer'), function (i, el) {
                        var $el = $(el);
                        var product = $el.data('product');

                        if (product) {
                            product.wc_product_price = data.price;
                            $(el).data('product', product);
                        }

                    });
                });
            })(jQuery);
		</script>
		<?php
	}

	public function run() {

		add_action( 'wp_head', array( $this, 'addCompatibilityScript' ) );


		add_filter( 'tiered_pricing_table/cart/product_cart_price', array( $this, 'addAddonsPriceToItem' ), 20, 2 );
		add_filter( 'tiered_pricing_table/cart/product_cart_regular_price/item', array(
			$this,
			'addAddonsPriceToItem',
		), 20, 2 );
		add_filter( 'tiered_pricing_table/cart/product_cart_price/item', array(
			$this,
			'addAddonsPriceToItem',
		), 20, 2 );
		add_filter( 'tiered_pricing_table/cart/product_cart_old_price', array( $this, 'addAddonsPriceToItem' ), 20, 2 );
	}
}
