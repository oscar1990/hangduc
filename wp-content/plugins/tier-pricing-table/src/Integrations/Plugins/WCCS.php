<?php namespace TierPricingTable\Integrations\Plugins;

class WCCS extends PluginIntegrationAbstract {

	public function run() {

		add_filter( 'tiered_pricing_table/price/price_by_rules',
			function ( $product_price, $quantity, $product_id, $context ) {

				global $WCCS;

				if ( $WCCS ) {

					$product = wc_get_product( $product_id );

					if ( $context === "view" && $product ) {
						return $WCCS->wccs_custom_price( $product_price, $product );
					}
				}

				return $product_price;

			}, 10, 10 );
	}

	public function getTitle() {
		return __( 'WooCommerce Currency Switcher by WP Experts (WCCS)', 'tier-pricing-table' );
	}

	public function getDescription() {
		return __( 'Integration provides compatibility with WooCommerce Currency Switcher by WP Experts to properly work with multiple currencies.',
			'tier-pricing-table' );
	}

	public function getSlug() {
		return 'wccs';
	}

	public function isPluginActive() {
		return true;
	}
}
