<?php namespace TierPricingTable\Integrations\Plugins;

use TierPricingTable\PricingRule;

class WOOCS extends PluginIntegrationAbstract {

	public function run() {
		add_filter( 'tiered_pricing_table/price/price_by_rules',
			function ( $product_price, $quantity, $product_id, $context ) {
				global $WOOCS_STARTER;

				if ( $WOOCS_STARTER ) {
					if ( $context === "view" ) {
						return $WOOCS_STARTER->get_actual_obj()->raw_woocommerce_price( $product_price );
					}
				}

				return $product_price;

			}, 10, 10 );

		// temporary disabled
		add_filter( '__tiered_pricing_table/price/pricing_rule', function ( PricingRule $pricingRule ) {
			global $WOOCS_STARTER;

			if ( $pricingRule->isFixed() && $WOOCS_STARTER ) {
				$pricingRules = array_map( function ( $el ) use ( $WOOCS_STARTER ) {
					return $WOOCS_STARTER->get_actual_obj()->raw_woocommerce_price( $el );
				}, $pricingRule->getRules() );

				$pricingRule->setRules( $pricingRules );
			}

			return $pricingRule;
		}, 49, 2 );
	}

	public function getTitle() {
		return __( 'WooCommerce Currency Switcher (FOX)', 'tier-pricing-table' );
	}

	public function getDescription() {
		return __( 'Integration provides compatibility with WooCommerce Currency Switcher to properly work with multiple currencies.',
			'tier-pricing-table' );
	}

	public function getSlug() {
		return 'woocs';
	}

	public function isPluginActive() {
		return true;
	}
}
