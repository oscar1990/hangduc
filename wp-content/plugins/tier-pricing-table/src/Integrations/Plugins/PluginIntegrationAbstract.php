<?php namespace TierPricingTable\Integrations\Plugins;

use TierPricingTable\Core\ServiceContainerTrait;
use TierPricingTable\Settings\CustomOptions\TPTSwitchOption;
use TierPricingTable\Settings\Settings;

abstract class PluginIntegrationAbstract {

	use ServiceContainerTrait;

	abstract public function getTitle();

	abstract public function getDescription();

	abstract public function getSlug();

	abstract public function isPluginActive();

	abstract public function run();

	public function __construct() {

		add_filter( 'tiered_pricing_table/settings/integrations_settings', array(
			$this,
			'addToIntegrationsSettings'
		) );

		if ( $this->isEnabled() ) {
			$this->run();
		}
	}

	public function addToIntegrationsSettings( $integrations ) {
		$integrations[] = array(
			'title'            => $this->getTitle(),
			'id'               => Settings::SETTINGS_PREFIX . '_integration_' . $this->getSlug(),
			'default'          => $this->isActiveByDefault() ? 'yes' : 'no',
			'is_plugin_active' => $this->isPluginActive(),
			'desc'             => $this->getDescription(),
			'type'             => TPTSwitchOption::FIELD_TYPE,
		);

		return $integrations;
	}

	public function isEnabled() {
		return $this->getContainer()->getSettings()->get( '_integration_' . $this->getSlug(), 'yes' ) === 'yes';
	}

	protected function isActiveByDefault() {
		return true;
	}
}