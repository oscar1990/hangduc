<?php namespace TierPricingTable\Integrations\Plugins\Elementor;

use Elementor\Plugin;
use TierPricingTable\Integrations\Plugins\PluginIntegrationAbstract;

class ElementorIntegration extends PluginIntegrationAbstract {

	public function getTitle() {
		return __( 'Elementor', 'tier-pricing-table' );
	}

	public function getDescription() {
		return __( 'Integration provides three tiered pricing widgets (table, blocks and options) with a bunch of settings.', 'tier-pricing-table' );
	}

	public function getSlug() {
		return 'elementor';
	}

	public function isPluginActive() {
		return true;
	}

	public function run() {
		add_action( 'elementor/widgets/widgets_registered', function () {
			Plugin::instance()->widgets_manager->register( new ElementorWidget() );
		} );
	}
}