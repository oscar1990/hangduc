<?php namespace TierPricingTable\Settings\Sections\Integrations;

use TierPricingTable\Core\ServiceContainerTrait;
use TierPricingTable\Settings\CustomOptions\TPTSwitchOption;
use TierPricingTable\Settings\Sections\SectionAbstract;
use TierPricingTable\Settings\Settings;

class IntegrationSection extends SectionAbstract {

	use ServiceContainerTrait;

	public function getSettings() {
		$settings     = array();
		$integrations = apply_filters( 'tiered_pricing_table/settings/integrations_settings', array() );

		$sectionTitle = array(
			'title' => __( 'Integrations', 'tier-pricing-table' ),
			'desc'  => __( 'This section controls all tiered pricing build-in integrations',
				'tier-pricing-table' ),
			'id'    => Settings::SETTINGS_PREFIX . 'integrations',
			'type'  => 'title',
		);

		$sectionEnd = array(
			'type' => 'sectionend',
			'id'   => Settings::SETTINGS_PREFIX . 'integrations'
		);

		$settings[] = $sectionTitle;
		$settings   = array_merge( $settings, $integrations );
		$settings[] = $sectionEnd;

		return $settings;

	}

	public function getSlug() {
		return 'integrations';
	}

	public function getName() {
		return __( 'Integrations', 'tier-pricing-table' );
	}

	public static function deleteOptions() {
		delete_option( Settings::SETTINGS_PREFIX . "table_integrations" );
		delete_option( Settings::SETTINGS_PREFIX . "_integration_elementor" );
		delete_option( Settings::SETTINGS_PREFIX . "_integration_wpallimport" );
		delete_option( Settings::SETTINGS_PREFIX . "_integration_mix-match-for-woocommerce" );
		delete_option( Settings::SETTINGS_PREFIX . "_integration_product-add-ons" );
		delete_option( Settings::SETTINGS_PREFIX . "_integration_woocommerce-deposits" );
		delete_option( Settings::SETTINGS_PREFIX . "_integration_product-bundles-for-woocommerce" );
		delete_option( Settings::SETTINGS_PREFIX . "_integration_aelia-multicurrency" );
		delete_option( Settings::SETTINGS_PREFIX . "_integration_wcpa" );
		delete_option( Settings::SETTINGS_PREFIX . "integrations" );
	}
}