<?php namespace TierPricingTable\Settings\Sections;

use TierPricingTable\Settings\Settings;

abstract class SectionAbstract {

	abstract public function getName();

	abstract public function getSlug();

	abstract public function getSettings();

	public function isActive() {

		if ( isset( $_GET['section'] ) ) {
			return isset( $_GET['section'] ) && $_GET['section'] === $this->getSlug();
		} else {
			return $this->getSlug() === Settings::DEFAULT_SECTION;
		}
	}

	public function getURL() {
		return add_query_arg( array( 'section' => $this->getSlug() ) );
	}

}