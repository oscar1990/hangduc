<?php namespace TierPricingTable\Settings\Sections\GeneralSection\Subsections;

use TierPricingTable\Settings\CustomOptions\TPTSwitchOption;
use TierPricingTable\Settings\Sections\SubsectionAbstract;
use TierPricingTable\Settings\Settings;

class SummarySubsection extends SubsectionAbstract {

	public function getTitle() {
		return __( 'Totals', 'tier-pricing-table' );
	}

	public function getDescription() {
		return __( 'Section controls how totals block at a product page.', 'tier-pricing-table' );
	}

	public function getSlug() {
		return 'summary';
	}

	public function getSettings() {
		return array(
			array(
				'title'    => __( 'Show totals', 'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'display_summary',
				'type'     => TPTSwitchOption::FIELD_TYPE,
				'default'  => 'yes',
				'desc'     => __( 'Show totals on the product page. Display information about actual unit price, product quantity and total',
					'tier-pricing-table' ),
				'desc_tip' => true,
			),
			array(
				'title'    => __( 'Totals title', 'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'summary_title',
				'type'     => 'text',
				'desc'     => __( 'The name is displaying above the summary block.', 'tier-pricing-table' ),
				'desc_tip' => true,
				'default'  => '',
			),
			array(
				'title'    => __( 'Totals type', 'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'summary_type',
				'type'     => 'select',
				'options'  => array(
					'table'  => __( 'Table', 'tier-pricing-table' ),
					'inline' => __( 'Inline', 'tier-pricing-table' ),
				),
				'desc'     => __( 'Type of displaying.', 'tier-pricing-table' ),
				'desc_tip' => true,
				'default'  => 'table',
			),
			array(
				'title'    => __( '"Total" label', 'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'summary_total_label',
				'type'     => 'text',
				'default'  => __( 'Total:', 'tier-pricing-table' ),
				'desc'     => __( 'Label for the "total" line.', 'tier-pricing-table' ),
				'desc_tip' => true,
			),
			array(
				'title'    => __( '"Each" label', 'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'summary_each_label',
				'type'     => 'text',
				'default'  => __( 'Each: ', 'tier-pricing-table' ),
				'desc'     => __( 'Label for the "each" line.', 'tier-pricing-table' ),
				'desc_tip' => true,
			),
			array(
				'title'    => __( 'Totals position', 'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'summary_position_hook',
				'type'     => 'select',
				'options'  => array(
					'woocommerce_before_add_to_cart_button'     => __( 'Above buy button', 'tier-pricing-table' ),
					'woocommerce_after_add_to_cart_button'      => __( 'Below buy button', 'tier-pricing-table' ),
					'woocommerce_before_add_to_cart_form'       => __( 'Above add to cart form', 'tier-pricing-table' ),
					'woocommerce_after_add_to_cart_form'        => __( 'Below add to cart form', 'tier-pricing-table' ),
					'woocommerce_single_product_summary'        => __( 'Above product title', 'tier-pricing-table' ),
					'woocommerce_before_single_product_summary' => __( 'Before product summary', 'tier-pricing-table' ),
					'woocommerce_after_single_product_summary'  => __( 'After product summary', 'tier-pricing-table' ),
				),
				'default'  => 'woocommerce_after_add_to_cart_button',
				'desc'     => __( 'Where to display the summary block.', 'tier-pricing-table' ),
				'desc_tip' => true,
			),
		);
	}
}