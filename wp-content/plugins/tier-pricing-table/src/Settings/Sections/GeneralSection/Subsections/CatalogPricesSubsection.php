<?php namespace TierPricingTable\Settings\Sections\GeneralSection\Subsections;

use TierPricingTable\Settings\CustomOptions\TPTDisplayType;
use TierPricingTable\Settings\CustomOptions\TPTSwitchOption;
use TierPricingTable\Settings\Sections\SubsectionAbstract;
use TierPricingTable\Settings\Settings;

class CatalogPricesSubsection extends SubsectionAbstract {

	public function getTitle() {
		return __( 'Tiered pricing catalog prices', 'tier-pricing-table' );
	}

	public function getDescription() {
		return __( 'This section controls how the tiered pricing will look on catalog, widgets and everywhere where a product price is displayed',
			'tier-pricing-table' );
	}

	public function getSlug() {
		return 'catalog_prices';
	}

	public function getSettings() {
		return array(
			array(
				'title'    => __( 'Price formatting on the catalog page, widgets, etc',
					'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'tiered_price_at_catalog',
				'type'     => TPTSwitchOption::FIELD_TYPE,
				'default'  => 'yes',
				'desc'     => __( 'Change price formatting on the whole site (show range or minimal price)',
					'tier-pricing-table' ),
				'desc_tip' => true,
			),
			array(
				'title'    => __( 'Enable catalog, widget price formatting for variable products',
					'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'tiered_price_at_catalog_for_variable',
				'type'     => TPTSwitchOption::FIELD_TYPE,
				'default'  => 'yes',
				'desc'     => __( 'Show tiered price at the catalog for variable products. Uses the lowest and the highest prices from all variations',
					'tier-pricing-table' ),
				'desc_tip' => true,
			),
			array(
				'title'    => __( 'Display Tiered Price as', 'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'tiered_price_at_catalog_type',
				'type'     => 'select',
				'options'  => [
					'range'  => __( 'Range (from lowest to highest)', 'tier-pricing-table' ),
					'lowest' => __( 'Lowest price', 'tier-pricing-table' ),
				],
				'desc'     => __( 'How to Display Tiered Price at The Catalog', 'tier-pricing-table' ),
				'desc_tip' => true,
			),
			array(
				'title'    => __( 'Lowest price prefix', 'tier-pricing-table' ),
				'id'       => Settings::SETTINGS_PREFIX . 'lowest_prefix',
				'type'     => 'text',
				'default'  => __( 'From', 'tier-pricing-table' ),
				'desc'     => __( 'Prefix Before Lowest Tiered Product Price at the Catalog. Example: <b>From 10$</b>',
					'tier-pricing-table' ),
				'desc_tip' => true,
			)
		);
	}
}