<?php namespace TierPricingTable\Settings\Sections\Advanced;

use TierPricingTable\Cache;
use TierPricingTable\Settings\CustomOptions\TPTLinkButton;
use TierPricingTable\Settings\CustomOptions\TPTSwitchOption;
use TierPricingTable\Settings\Sections\SectionAbstract;
use TierPricingTable\Settings\Settings;

class AdvancedSection extends SectionAbstract {

	public function getSettings() {

		$settings = array();
		$advanced = apply_filters( 'tiered_pricing_table/settings/advanced_settings', array() );

		$sectionTitle = array(
			'title' => __( 'Additional features', 'tier-pricing-table' ),
			'desc'  => __( 'This section controls all advanced tiered pricing features',
				'tier-pricing-table' ),
			'id'    => Settings::SETTINGS_PREFIX . 'advanced',
			'type'  => 'title',
		);

		$sectionEnd = array(
			'type' => 'sectionend',
			'id'   => Settings::SETTINGS_PREFIX . 'advanced'
		);

		$settings[] = $sectionTitle;
		$settings   = array_merge( $settings, $advanced );
		$settings[] = $sectionEnd;

		$settings = array_merge( $settings, $this->getCacheSettings() );

		return $settings;
	}

	public function getSlug() {
		return 'advanced';
	}

	public function getName() {
		return __( 'Advanced', 'tier-pricing-table' );
	}

	public static function deleteOptions() {
		delete_option( Settings::SETTINGS_PREFIX . "advanced" );
		delete_option( Settings::SETTINGS_PREFIX . "_addon_category-tiered-pricing" );
		delete_option( Settings::SETTINGS_PREFIX . "_addon_manual-orders" );
		delete_option( Settings::SETTINGS_PREFIX . "_addon_role-based-rules" );
		delete_option( Settings::SETTINGS_PREFIX . "_addon_global-tier-pricing" );
		delete_option( Settings::SETTINGS_PREFIX . "_addon_minimum-quantity" );
		delete_option( Settings::SETTINGS_PREFIX . "advanced" );
	}

	protected function getCacheSettings() {
		return array(
			array(
				'title' => __( 'Cache', 'tier-pricing-table' ),
				'desc'  => __( 'Cache improves performance making the plugin not calculate data on each request. Disable to debug issues.', 'tier-pricing-table' ),
				'id'    => Settings::SETTINGS_PREFIX . 'advanced_cache',
				'type'  => 'title',
			),
			array(
				'title'   => __( 'Enabled', 'tier-pricing-table' ),
				'id'      => Settings::SETTINGS_PREFIX . 'cache_enabled',
				'type'    => TPTSwitchOption::FIELD_TYPE,
				'default' => 'yes',
			),
			array(
				'title'        => __( 'Purge', 'tier-pricing-table' ),
				'button_text'  => __( 'Purge cache', 'tier-pricing-table' ),
				'button_class' => 'button button-large',
				'button_link'  => Cache::getPurgeURL(),
				'type'         => TPTLinkButton::FIELD_TYPE,
			),
			array(
				'type' => 'sectionend',
				'id'   => Settings::SETTINGS_PREFIX . 'advanced'
			)
		);
	}
}