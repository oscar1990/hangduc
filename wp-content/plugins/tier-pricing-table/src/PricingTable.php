<?php namespace TierPricingTable;

use MeowCrew\RoleAndCustomerBasedPricing\Admin\ProductPage\PricingRulesManager;
use TierPricingTable\Admin\ProductManagers\AdvanceOptionsForVariableProduct;
use TierPricingTable\Admin\ProductManagers\ProductManager;
use TierPricingTable\Core\ServiceContainerTrait;
use TierPricingTable\Settings\Sections\GeneralSection\GeneralSection;
use WC_Product;
use WC_Product_Variable;
use WC_Product_Variation;

class PricingTable {

	use ServiceContainerTrait;

	private static $instance;

	private function __construct() {
	}

	public static function getInstance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Main function for rendering pricing table for product
	 *
	 * @param int $parentProductID
	 * @param int $variationID
	 * @param array $settings
	 */
	public function renderPricingTable( $parentProductID = 0, $variationID = null, $settings = array() ) {

		$parentProduct = wc_get_product( $parentProductID );

		if ( ! $parentProduct ) {
			return;
		}

		if ( ! $this->productHasPricingRules( $parentProduct ) ) {
			return;
		}

		$settings = wp_parse_args( $settings, $this->getDefaultSettings( $parentProductID ) );

		// If the product is variable, but no concrete variation is passed - check for default
		if ( ! $variationID && in_array( $parentProduct->get_type(), TierPricingTablePlugin::getSupportedVariableProductTypes() ) ) {
			$defaultVariationId = $this->getDefaultVariation( $parentProduct );
			$variationID        = $defaultVariationId ? $defaultVariationId : null;
		}

		// Variation if exist. If not the parent product should be used
		$productId = $variationID ? $variationID : $parentProduct->get_id();
		$product   = wc_get_product( $productId );

		$supportedTypes = array_merge( TierPricingTablePlugin::getSupportedSimpleProductTypes(),
			TierPricingTablePlugin::getSupportedVariableProductTypes() );

		// Exit if product is not valid
		if ( ! $product || ! in_array( $parentProduct->get_type(), $supportedTypes ) ) {
			return;
		}

		if ( $settings['display_type'] === 'tooltip' ) {
			wp_enqueue_script( 'jquery-ui-tooltip' );
		}

		$hidden = ( 'tooltip' === $settings['display_type'] || ! $settings['display'] );

		?>
        <div class="clear"></div>
        <div class="tpt__tiered-pricing <?php echo esc_attr( $hidden ? 'tpt__hidden' : '' ); ?>"
             data-settings="<?php echo esc_attr( json_encode( $settings ) ); ?>"
             data-display-type="<?php echo esc_attr( $settings['display_type'] ) ?>"
             data-product-type="<?php echo esc_attr( $parentProduct->get_type() ); ?>">
			<?php $this->renderPricingTableHTML( $parentProduct, $product, $settings ); ?>
        </div>
		<?php
	}

	/**
	 * Render pricing table without wrapper
	 *
	 * @param WC_Product $parentProduct
	 * @param WC_Product $product
	 * @param array $settings
	 */
	public function renderPricingTableHTML( WC_Product $parentProduct, WC_Product $product, $settings = array() ) {

		$settings = wp_parse_args( $settings, $this->getDefaultSettings( $parentProduct->get_id() ) );

		// Don't get rules from variable product when variation is empty
		if ( in_array( $parentProduct->get_type(), TierPricingTablePlugin::getSupportedVariableProductTypes() ) && ! ( $product instanceof WC_Product_Variation ) ) {
			// Empty rule
			$priceRule = new PricingRule( $product->get_id() );
		} else {
			$priceRule = PriceManager::getPricingRule( $product->get_id() );
		}

		// If rules are not empty
		if ( ! empty( $priceRule->getRules() ) ) {

			$displayType = $settings['display_type'];

			$templateType = in_array( $displayType, array(
				'blocks',
				'table',
				'options',
				'dropdown',
				'tooltip'
			) ) ? $displayType : 'table';

			if ( $displayType === 'tooltip' ) {
				$templateType = 'table';
			}

			$template = "tiered-pricing-${templateType}.php";

			$this->getContainer()->getFileManager()->includeTemplate( 'frontend/' . $template, array(
				'price_rules'  => $priceRule->getRules(),
				'real_price'   => $product->get_price(),
				'product_name' => $product->get_name(),
				'product_id'   => $product->get_id(),
				'product'      => $product,
				'minimum'      => $priceRule->getMinimum(),
				'settings'     => $settings,
				'pricing_type' => $priceRule->getType(),
				'id'           => $this->getUniqueTieredPricingId()
			) );
		}
	}

	protected function getDefaultSettings( $productId = false ) {
		$settings = array(
			'display'               => $this->getContainer()->getSettings()->get( 'display', 'yes' ) === 'yes',
			'display_type'          => $this->getContainer()->getSettings()->get( 'display_type', 'table' ),
			'title'                 => $this->getContainer()->getSettings()->get( 'table_title', '' ),
			'table_class'           => $this->getContainer()->getSettings()->get( 'table_css_class', '' ),
			'quantity_column_title' => $this->getContainer()->getSettings()->get( 'head_quantity_text', __( 'Quantity', 'tier-pricing-table' ) ),
			'price_column_title'    => $this->getContainer()->getSettings()->get( 'head_price_text', __( 'Price', 'tier-pricing-table' ) ),
			'discount_column_title' => $this->getContainer()->getSettings()->get( 'head_discount_text', __( 'Discount (%)', 'tier-pricing-table' ) ),
			'quantity_type'         => $this->getContainer()->getSettings()->get( 'quantity_type', 'range' ),
			'show_discount_column'  => $this->getContainer()->getSettings()->get( 'show_discount_column', 'yes' ) === 'yes',
			'clickable_rows'        => $this->getContainer()->getSettings()->get( 'clickable_table_rows', 'yes' ) === 'yes',
			'active_tier_color'     => $this->getContainer()->getSettings()->get( 'selected_quantity_color', '#96598A' ),
			'tooltip_border'        => $this->getContainer()->getSettings()->get( 'tooltip_border', 'yes' ) === 'yes',

			'options_show_total'                  => GeneralSection::isShowOptionTotal(),
			'options_show_original_product_price' => GeneralSection::isShowOriginalProductPrice(),
			'options_show_default_option'         => GeneralSection::isDefaultOptionEnabled(),

			'options_default_option_text' => GeneralSection::getDefaultOptionText(),
			'options_option_text'         => GeneralSection::getOptionText(),

			'update_price_on_product_page'  => $this->getContainer()->getSettings()->get( 'update_price_on_product_page', 'yes' ) === 'yes',
			'show_tiered_price_as_discount' => $this->getContainer()->getSettings()->get( 'show_tiered_price_as_discount', 'yes' ) === 'yes',
			'show_total_price'              => $this->getContainer()->getSettings()->get( 'show_total_price', 'no' ) === 'yes',
		);

		$default_quantity_measurement = array(
			'singular' => '',
			'plural'   => '',
		);

		$quantity_measurement = $default_quantity_measurement;

		if ( $settings['display_type'] === 'table' ) {
			$quantity_measurement = $this->getContainer()->getSettings()->get( 'table_quantity_measurement', $default_quantity_measurement );
		}
		if ( $settings['display_type'] === 'blocks' ) {
			$quantity_measurement = $this->getContainer()->getSettings()->get( 'blocks_quantity_measurement', array(
				'singular' => _n( 'piece', 'pieces', 1, 'tier-pricing-table' ),
				'plural'   => _n( 'piece', 'pieces', 2, 'tier-pricing-table' ),
			) );
		}

		$settings['quantity_measurement_singular'] = $quantity_measurement['singular'];
		$settings['quantity_measurement_plural']   = $quantity_measurement['plural'];

		if ( $productId ) {
			$template = ProductManager::getProductTemplate( $productId );

			if ( $template !== 'default' ) {
				$settings['display_type'] = $template;
			}
		}

		return $settings;
	}

	protected function getUniqueTieredPricingId() {
		return preg_replace( '/[0-9]+/', '', strtolower( wp_generate_password( 20, false ) ) );
	}

	protected function getDefaultVariation( WC_Product_Variable $product ) {

		$defaultVariation = AdvanceOptionsForVariableProduct::getDefaultVariation( $product->get_id() );

		if ( $defaultVariation ) {
			return $defaultVariation->get_id();
		}

		$cached = Cache::getCachedDataForVariableProduct( $product->get_id(), 'default_variation_id' );

		if ( $cached !== false ) {
			return $cached;
		}

		$variation_id         = 0;
		$is_default_variation = false;

		foreach ( $product->get_available_variations() as $variation_values ) {
			foreach ( $variation_values['attributes'] as $key => $attribute_value ) {
				$attribute_name = str_replace( 'attribute_', '', $key );
				$default_value  = $product->get_variation_default_attribute( $attribute_name );
				if ( $default_value == $attribute_value ) {
					$is_default_variation = true;
				} else {
					$is_default_variation = false;
					break;
				}
			}
			if ( $is_default_variation ) {
				$variation_id = $variation_values['variation_id'];
				break;
			}
		}

		Cache::setCachedDataForVariableProduct( $product->get_id(), 'default_variation_id', $variation_id );

		return $variation_id ? $variation_id : false;
	}

	public function productHasPricingRules( WC_Product $product ) {

		if ( TierPricingTablePlugin::isSimpleProductSupported( $product ) ) {
			$pricingRule = PriceManager::getPricingRule( $product->get_id() );

			return ! empty( $pricingRule->getRules() );
		}

		if ( TierPricingTablePlugin::isVariableProductSupported( $product ) && $product instanceof WC_Product_Variable ) {

			if ( AdvanceOptionsForVariableProduct::productHasNoRules( $product->get_id() ) ) {
				return false;
			}

			$hasTieredPricing = Cache::getCachedDataForVariableProduct( $product->get_id(), 'product_has_rules' );

			if ( $hasTieredPricing === 'no' ) {
				return false;
			}

			if ( $hasTieredPricing === 'yes' ) {
				return true;
			}

			foreach ( $product->get_available_variations() as $productVariation ) {
				$pricingRule = PriceManager::getPricingRule( $productVariation['variation_id'] );

				if ( ! empty( $pricingRule->getRules() ) ) {
					Cache::setCachedDataForVariableProduct( $product->get_id(), 'product_has_rules', 'yes' );

					return true;
				}
			}

			Cache::setCachedDataForVariableProduct( $product->get_id(), 'product_has_rules', 'no' );
		}

		return false;
	}
}