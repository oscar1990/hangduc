<?php

namespace TierPricingTable;

use  Automattic\WooCommerce\Utilities\FeaturesUtil ;
use  TierPricingTable\Addons\Addons ;
use  TierPricingTable\API\WooCommerceRESTAPI ;
use  TierPricingTable\Core\FileManager ;
use  TierPricingTable\Core\AdminNotifier ;
use  TierPricingTable\Core\ServiceContainerTrait ;
use  TierPricingTable\Frontend\CartPriceManager ;
use  TierPricingTable\Frontend\CartUpsellManager ;
use  TierPricingTable\Frontend\CatalogPriceManager ;
use  TierPricingTable\Settings\Settings ;
use  TierPricingTable\Admin\Admin ;
use  TierPricingTable\Frontend\Frontend ;
use  WC_Product ;
/**
 * Class TierPricingTablePlugin
 *
 * @package TierPricingTable
 */
class TierPricingTablePlugin
{
    use  ServiceContainerTrait ;
    /**
     * @var Freemius
     */
    private  $licence ;
    /**
     * @var Integrations\Integrations
     */
    private  $integrations ;
    /**
     * @var Addons
     */
    private  $addons ;
    const  VERSION = '3.5.1' ;
    /**
     * TierPricingTablePlugin constructor.
     *
     * @param  string  $mainFile
     */
    public function __construct( $mainFile )
    {
        $this->getContainer()->add( 'fileManager', new FileManager( $mainFile ) );
        $this->getContainer()->add( 'adminNotifier', new AdminNotifier() );
        $this->getContainer()->add( 'settings', new Settings() );
        $this->licence = new Freemius( $mainFile );
        $this->integrations = new Integrations\Integrations();
        $this->addons = new Addons();
        add_action( 'init', array( $this, 'loadTextDomain' ), -999 );
        add_action( 'admin_init', array( $this, 'checkRequirePlugins' ) );
        add_filter(
            'plugin_action_links_' . plugin_basename( $this->getContainer()->getFileManager()->getMainFile() ),
            array( $this, 'addPluginAction' ),
            10,
            4
        );
        add_action( 'before_woocommerce_init', function () use( $mainFile ) {
            if ( class_exists( FeaturesUtil::class ) ) {
                FeaturesUtil::declare_compatibility( 'custom_order_tables', $mainFile, true );
            }
        } );
    }
    
    /**
     * Add setting to plugin actions at plugins list
     *
     * @param  array  $actions
     *
     * @return array
     */
    public function addPluginAction( $actions )
    {
        $actions[] = '<a href="' . $this->getContainer()->getSettings()->getLink() . '">' . __( 'Settings', 'tier-pricing-table' ) . '</a>';
        $actions[] = '<a target="_blank" href="' . self::getDocumentationURL() . '">' . __( 'Documentation', 'tier-pricing-table' ) . '</a>';
        if ( !tpt_fs()->is_anonymous() && tpt_fs()->is_installed_on_site() ) {
            $actions[] = '<a href="' . self::getAccountPageURL() . '"><b style="color: green">' . __( 'Account', 'tier-pricing-table' ) . '</b></a>';
        }
        $actions[] = '<a href="' . self::getContactUsURL() . '"><b style="color: green">' . __( 'Contact Us', 'tier-pricing-table' ) . '</b></a>';
        if ( !tpt_fs()->is_premium() ) {
            $actions[] = '<a href="' . tpt_fs_activation_url() . '"><b style="color: red">' . __( 'Go Premium', 'tier-pricing-table' ) . '</b></a>';
        }
        return $actions;
    }
    
    /**
     * Run plugin part
     */
    public function run()
    {
        $valid = count( $this->validateRequiredPlugins() ) === 0 && $this->licence->isValid();
        
        if ( $valid ) {
            new Frontend();
            new Admin();
            new CartPriceManager();
            new WooCommerceRESTAPI();
            new LegacyHooks();
            Cache::init();
        }
    
    }
    
    /**
     * Load plugin translations
     */
    public function loadTextDomain()
    {
        $name = $this->getContainer()->getFileManager()->getPluginName();
        load_plugin_textdomain( 'tier-pricing-table', false, $name . '/languages/' );
    }
    
    /**
     * Validate required plugins
     *
     * @return array
     */
    private function validateRequiredPlugins()
    {
        $plugins = array();
        if ( !function_exists( 'is_plugin_active' ) ) {
            include_once ABSPATH . 'wp-admin/includes/plugin.php';
        }
        /**
         * Check if WooCommerce is active
         **/
        if ( !(is_plugin_active( 'woocommerce/woocommerce.php' ) || is_plugin_active_for_network( 'woocommerce/woocommerce.php' )) ) {
            $plugins[] = '<a target="_blank" href="https://wordpress.org/plugins/woocommerce/">WooCommerce</a>';
        }
        return $plugins;
    }
    
    /**
     * Check required plugins and push notifications
     */
    public function checkRequirePlugins()
    {
        /* translators: %s: required plugin */
        $message = __( 'The Tiered Pricing Table plugin requires %s plugin to be active!', 'tier-pricing-table' );
        $plugins = $this->validateRequiredPlugins();
        if ( count( $plugins ) ) {
            foreach ( $plugins as $plugin ) {
                $error = sprintf( $message, $plugin );
                $this->getContainer()->getAdminNotifier()->push( $error, AdminNotifier::ERROR, false );
            }
        }
    }
    
    /**
     * Fired during plugin uninstall
     */
    public static function uninstall()
    {
        Settings::deleteOptions();
    }
    
    public static function getSupportedSimpleProductTypes()
    {
        return apply_filters( 'tiered_pricing_table/supported_simple_product_types', array( 'simple', 'subscription', 'variation' ) );
    }
    
    public static function getSupportedVariableProductTypes()
    {
        return apply_filters( 'tiered_pricing_table/supported_variable_product_types', array( 'variable', 'variable-subscription' ) );
    }
    
    public static function isSimpleProductSupported( WC_Product $product )
    {
        return in_array( $product->get_type(), self::getSupportedSimpleProductTypes() );
    }
    
    public static function isVariableProductSupported( WC_Product $product )
    {
        return in_array( $product->get_type(), self::getSupportedVariableProductTypes() );
    }
    
    /**
     * Plugin activation
     */
    public function activate()
    {
        set_transient( 'tiered_pricing_table_activated', 'yes', 100 );
    }
    
    public static function getDocumentationURL()
    {
        return 'https://woocommerce.com/document/tiered-pricing-table/';
    }
    
    public static function getContactUsURL()
    {
        return admin_url( 'admin.php?page=tiered-pricing-table-contact-us' );
    }
    
    public static function getAccountPageURL()
    {
        return admin_url( 'admin.php?page=tired-pricing-table-account' );
    }
    
    /**
     * Uses for separate rules during the import
     *
     * @return string
     */
    public static function getRulesSeparator()
    {
        return apply_filters( 'tiered_pricing_table/rules_separator', ',' );
    }

}