<?php namespace TierPricingTable;

class PricingRule {

	protected $minimum;
	protected $rules = array();
	protected $type;
	protected $productId;
	protected $pricingProvider = 'product';

	/**
	 * PricingRule constructor.
	 *
	 * @param $productId
	 */
	public function __construct( $productId ) {
		$this->productId = $productId;
	}

	/**
	 * @return mixed
	 */
	public function getProductId() {
		return $this->productId;
	}

	/**
	 * @return int
	 */
	public function getMinimum() {
		return $this->minimum;
	}

	/**
	 * @param  int  $minimum
	 */
	public function setMinimum( int $minimum ) {
		$this->minimum = intval( $minimum );
	}

	/**
	 * @return array
	 */
	public function getRules() {
		return $this->rules;
	}

	/**
	 * @param  array  $rules
	 */
	public function setRules( array $rules ) {
		$this->rules = $rules;
	}

	/**
	 * @return mixed
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param  mixed  $type
	 */
	public function setType( $type ) {
		$this->type = in_array( $type, array( 'fixed', 'percentage' ) ) ? $type : 'fixed';
	}

	public function isPercentage() {
		return $this->getType() === 'percentage';
	}

	public function isFixed() {
		return $this->getType() === 'fixed';
	}

	/**
	 * @return string
	 */
	public function getPricingProvider() {
		return $this->pricingProvider;
	}

	/**
	 * @param  string  $pricingProvider
	 */
	public function setPricingProvider( $pricingProvider ) {
		$this->pricingProvider = $pricingProvider;
	}
}