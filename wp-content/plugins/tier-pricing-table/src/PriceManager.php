<?php

namespace TierPricingTable;

use  WC_Product ;
class PriceManager
{
    protected static  $types = array() ;
    protected static  $rules = array() ;
    /**
     * Return fixed price rules or empty array if not exist rules
     *
     * @param $productId
     * @param  string  $context
     *
     * @return array
     */
    public static function getFixedPriceRules( $productId, $context = 'view' )
    {
        return self::getPriceRules( $productId, 'fixed', $context );
    }
    
    /**
     * Return percentage price rules or empty array if not exist rules
     *
     * @param $productId
     * @param  string  $context
     *
     * @return array
     */
    public static function getPercentagePriceRules( $productId, $context = 'view' )
    {
        return self::getPriceRules( $productId, 'percentage', $context );
    }
    
    /**
     * Get product pricing rules
     *
     * @param  int  $productId
     * @param  bool  $type
     * @param  string  $context
     *
     * @return array
     */
    public static function getPriceRules( $productId, $type = false, $context = 'view' )
    {
        if ( $context !== 'edit' && array_key_exists( $productId, self::$rules ) ) {
            return self::$rules[$productId];
        }
        $type = ( $type ? $type : self::getPricingType( $productId, 'fixed', $context ) );
        
        if ( 'fixed' === $type ) {
            $rules = get_post_meta( $productId, '_fixed_price_rules', true );
        } else {
            $rules = get_post_meta( $productId, '_percentage_price_rules', true );
        }
        
        $parent_id = $productId;
        // If no rules for variation check for product level rules.
        
        if ( 'edit' !== $context && self::variationHasNoOwnRules( $productId, $rules ) ) {
            $product = wc_get_product( $productId );
            $parent_id = $product->get_parent_id();
            $type = self::getPricingType( $parent_id );
            
            if ( 'fixed' === $type ) {
                $rules = get_post_meta( $parent_id, '_fixed_price_rules', true );
            } else {
                $rules = get_post_meta( $parent_id, '_percentage_price_rules', true );
            }
        
        }
        
        $rules = ( !empty($rules) ? $rules : array() );
        ksort( $rules );
        
        if ( 'edit' !== $context ) {
            $rules = apply_filters(
                'tiered_pricing_table/price/product_price_rules',
                $rules,
                $productId,
                $type,
                $parent_id
            );
            // Cache
            self::$rules[$productId] = $rules;
        }
        
        return $rules;
    }
    
    /**
     * Get price by product quantity
     *
     * @param  int  $quantity
     * @param  int  $productId
     * @param  string  $context
     * @param  string  $place
     * @param  bool  $withTaxes
     *
     * @return bool|float|int
     */
    public static function getPriceByRules(
        $quantity,
        $productId,
        $context = 'view',
        $place = 'shop',
        $withTaxes = true
    )
    {
        $pricingRule = self::getPricingRule( $productId );
        foreach ( array_reverse( $pricingRule->getRules(), true ) as $_amount => $price ) {
            
            if ( $_amount <= $quantity ) {
                
                if ( $pricingRule->isPercentage() ) {
                    $product = wc_get_product( $productId );
                    if ( $product ) {
                        $productPrice = self::getPriceByPercentDiscount( $product->get_price(), $price );
                    }
                } else {
                    $productPrice = $price;
                }
                
                
                if ( 'view' === $context && $withTaxes ) {
                    $product = wc_get_product( $productId );
                    $productPrice = self::getPriceWithTaxes( $productPrice, $product, $place );
                }
                
                break;
            }
        
        }
        $productPrice = ( isset( $productPrice ) ? $productPrice : false );
        if ( $productPrice && apply_filters( 'tiered_pricing_table/price/round_price', true ) ) {
            $productPrice = round( $productPrice, max( 2, wc_get_price_decimals() ) );
        }
        return apply_filters(
            'tiered_pricing_table/price/price_by_rules',
            $productPrice,
            $quantity,
            $productId,
            $context,
            $place
        );
    }
    
    /**
     * Calculate displayed price depend on taxes
     *
     * @param  float  $price
     * @param  WC_Product  $product
     * @param  string  $place
     *
     * @return float
     */
    public static function getPriceWithTaxes( $price, $product, $place = 'shop' )
    {
        if ( wc_tax_enabled() ) {
            
            if ( 'cart' === $place ) {
                $price = ( 'incl' === get_option( 'woocommerce_tax_display_cart' ) ? wc_get_price_including_tax( $product, array(
                    'qty'   => 1,
                    'price' => $price,
                ) ) : wc_get_price_excluding_tax( $product, array(
                    'qty'   => 1,
                    'price' => $price,
                ) ) );
            } else {
                $price = wc_get_price_to_display( $product, array(
                    'price' => $price,
                    'qty'   => 1,
                ) );
            }
        
        }
        return $price;
    }
    
    /**
     * Calculate price using percentage discount
     *
     * @param  float|int  $price
     * @param  float|int  $discount
     *
     * @return bool|float|int
     */
    public static function getPriceByPercentDiscount( $price, $discount )
    {
        
        if ( $price > 0 && $discount <= 100 ) {
            $discount_amount = $price / 100 * $discount;
            return $price - $discount_amount;
        }
        
        return false;
    }
    
    /**
     * Get pricing type of product. Available: fixed or percentage
     *
     * @param  int  $productId
     * @param  string  $default
     * @param  string  $context
     *
     * @return string
     */
    public static function getPricingType( $productId, $default = 'fixed', $context = 'view' )
    {
        if ( $context !== 'edit' && array_key_exists( $productId, self::$types ) ) {
            return self::$types[$productId];
        }
        $type = 'fixed';
        $type = ( in_array( $type, array( 'fixed', 'percentage' ) ) ? $type : $default );
        
        if ( $context !== 'edit' ) {
            $type = apply_filters( 'tiered_pricing_table/price/type', $type, $productId );
            // Cache
            self::$types[$productId] = $type;
        }
        
        return $type;
    }
    
    /**
     * Update price rules
     *
     * @param  array  $amounts
     * @param  array  $prices
     * @param  int  $productId
     */
    public static function updateFixedPriceRules( $amounts, $prices, $productId )
    {
        $rules = array();
        foreach ( $amounts as $key => $amount ) {
            if ( !empty($amount) && !empty($prices[$key]) && !key_exists( $amount, $rules ) ) {
                $rules[$amount] = wc_format_decimal( $prices[$key] );
            }
        }
        update_post_meta( $productId, '_fixed_price_rules', $rules );
    }
    
    /**
     * Update price rules
     *
     * @param  array  $amounts
     * @param  array  $percents
     * @param  int  $productId
     */
    public static function updatePercentagePriceRules( $amounts, $percents, $productId )
    {
        $rules = array();
        foreach ( $amounts as $key => $amount ) {
            if ( !empty($amount) && !empty($percents[$key]) && !key_exists( $amount, $rules ) && $percents[$key] < 99 ) {
                $rules[$amount] = $percents[$key];
            }
        }
        update_post_meta( $productId, '_percentage_price_rules', $rules );
    }
    
    /**
     * Update product pricing type
     *
     * @param  int  $productId
     * @param  string  $type
     */
    public static function updatePriceRulesType( $productId, $type )
    {
        if ( in_array( $type, array( 'percentage', 'fixed' ) ) ) {
            update_post_meta( $productId, '_tiered_price_rules_type', $type );
        }
    }
    
    /**
     * Get minimum product qty for table
     *
     * @param  int  $productId
     * @param  string  $context
     *
     * @return int
     */
    public static function getProductQtyMin( $productId, $context = 'view' )
    {
        $currentProductId = $productId;
        $parentId = false;
        
        if ( 'view' === $context && self::variationHasNoOwnRules( $productId ) ) {
            $product = wc_get_product( $productId );
            $parentId = $product->get_parent_id();
            $currentProductId = $parentId;
        }
        
        $min = get_post_meta( $currentProductId, '_tiered_price_minimum_qty', true );
        $min = ( $min ? intval( $min ) : 1 );
        if ( 'view' === $context ) {
            return apply_filters(
                'tiered_pricing_table/price/minimum',
                $min,
                $productId,
                $parentId
            );
        }
        return $min;
    }
    
    /**
     * Check if variation has no own rules
     *
     * @param  int  $productId
     * @param  bool  $rules
     *
     * @return bool
     */
    protected static function variationHasNoOwnRules( $productId, $rules = false )
    {
        $rules = ( $rules ? $rules : self::getPriceRules( $productId, false, 'edit' ) );
        
        if ( empty($rules) ) {
            $product = wc_get_product( $productId );
            if ( $product ) {
                return $product->is_type( 'variation' );
            }
        }
        
        return false;
    }
    
    /**
     * Update product min
     *
     * @param  int  $productId
     * @param  int  $min
     */
    public static function updateProductQtyMin( $productId, $min )
    {
        $min = intval( $min );
        if ( $min > 0 ) {
            update_post_meta( $productId, '_tiered_price_minimum_qty', $min );
        }
    }
    
    public static function calculateDiscount( $originalPrice, $currentPrice )
    {
        if ( $currentPrice >= $originalPrice ) {
            return 0;
        }
        return 100 * ($originalPrice - $currentPrice) / $originalPrice;
    }
    
    /**
     * @param $productId
     *
     * @return PricingRule
     */
    public static function getPricingRule( $productId, $forceType = false )
    {
        $pricingRule = new PricingRule( $productId );
        
        if ( $forceType ) {
            
            if ( $forceType === 'percentage' ) {
                $pricingRule->setRules( self::getPercentagePriceRules( $productId ) );
                $pricingRule->setType( 'percentage' );
            } else {
                $pricingRule->setRules( self::getFixedPriceRules( $productId ) );
                $pricingRule->setType( 'fixed' );
            }
        
        } else {
            $pricingRule->setRules( self::getPriceRules( $productId ) );
            $pricingRule->setType( self::getPricingType( $productId ) );
        }
        
        $pricingRule->setMinimum( self::getProductQtyMin( $productId ) );
        return apply_filters( 'tiered_pricing_table/price/pricing_rule', $pricingRule, $productId );
    }

}