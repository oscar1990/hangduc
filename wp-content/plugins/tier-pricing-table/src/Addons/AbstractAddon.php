<?php namespace TierPricingTable\Addons;

use TierPricingTable\Core\ServiceContainerTrait;
use TierPricingTable\Settings\CustomOptions\TPTSwitchOption;
use TierPricingTable\Settings\Settings;

abstract class AbstractAddon {

	use ServiceContainerTrait;

	public function __construct() {
		add_filter( 'tiered_pricing_table/settings/advanced_settings', array(
			$this,
			'addToAddonsSettings'
		) );

	}

	public function addToAddonsSettings( $addons ) {
		$addons[] = array(
			'title'   => $this->getName(),
			'id'      => Settings::SETTINGS_PREFIX . '_addon_' . $this->getSlug(),
			'default' => $this->isActiveByDefaultString(),
			'desc'    => $this->getDescription(),
			'type'    => TPTSwitchOption::FIELD_TYPE,
		);

		return $addons;
	}

	public function isEnabled() {
		return $this->getContainer()->getSettings()->get( '_addon_' . $this->getSlug(), $this->isActiveByDefaultString() ) === 'yes';
	}

	protected function isActiveByDefault() {
		return true;
	}

	protected function isActiveByDefaultString() {
		return $this->isActiveByDefault() ? 'yes' : 'no';
	}

	abstract public function getName();

	abstract public function getDescription();

	abstract public function getSlug();

	abstract public function run();
}
