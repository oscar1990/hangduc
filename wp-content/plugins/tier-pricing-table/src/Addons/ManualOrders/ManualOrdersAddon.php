<?php namespace TierPricingTable\Addons\ManualOrders;

use TierPricingTable\Addons\AbstractAddon;
use TierPricingTable\PriceManager;
use WC_Order;
use WC_Order_Item;
use WC_Order_Item_Product;

class ManualOrdersAddon extends AbstractAddon {

	public function getName() {
		return __( 'Tiered pricing for manual orders', 'tier-pricing-table' );
	}

	public function run() {
		add_filter( 'woocommerce_ajax_order_item', array( $this, 'adjustOrderItemPriceSave' ), 3, 10 );
		add_action( 'woocommerce_before_save_order_item', array( $this, 'adjustOrderItemPriceUpdate' ), 1, 10 );
	}

	public function adjustOrderItemPriceSave( $item, $item_id, WC_Order $order ) {

		if ( $item instanceof WC_Order_Item_Product ) {

			$GLOBALS['tpt_current_user_id'] = $order->get_customer_id();

			$productId = $item->get_variation_id() ? $item->get_variation_id() : $item->get_product_id();
			$qty       = $item->get_quantity();
			$newPrice  = PriceManager::getPriceByRules( $qty, $productId, null, null, false );

			$product = $item->get_product() ? $item->get_product() : false;
			$total   = $qty * $newPrice;

			if ( $product ) {
				$newPrice = $newPrice ? $newPrice : $product->get_price();

				$total = wc_get_price_excluding_tax( $product, array(
					'price' => $newPrice,
					'qty'   => $qty
				) );
			}

			if ( $newPrice ) {

				foreach ( $order->get_items() as $_item ) {
					if ( $item->get_id() === $_item->get_id() && $_item instanceof WC_Order_Item_Product ) {
						$_item->set_total( $total );
						$_item->set_subtotal( $total );
						$_item->calculate_taxes();

						$_item->save();
					}
				}
			}
		}

		return $item;
	}

	public function adjustOrderItemPriceUpdate( WC_Order_Item $item ) {

		if ( $item instanceof WC_Order_Item_Product ) {

			$GLOBALS['tpt_current_user_id'] = $item->get_order()->get_customer_id();

			$productId = $item->get_variation_id() ? $item->get_variation_id() : $item->get_product_id();
			$qty       = $item->get_quantity();

			$product  = $item->get_product() ? $item->get_product() : false;
			$newPrice = PriceManager::getPriceByRules( $qty, $productId );

			$total = $qty * $newPrice;

			if ( $product ) {
				$newPrice = $newPrice ? $newPrice : $product->get_price();

				$total = wc_get_price_excluding_tax( $product, array(
					'price' => $newPrice,
					'qty'   => $qty
				) );
			}

			$item->set_subtotal( $total );

			if ( ! $this->isLineItemTotalManuallyChanged( $item->get_id() ) ) {
				$item->set_total( $total );
				$item->set_subtotal( $total );
			}

			$item->calculate_taxes();
			$item->save();
		}

		return $item;
	}

	public function isLineItemTotalManuallyChanged( $itemId ) {
		$data = $_REQUEST;

		$items = isset( $data['items'] ) ? wp_unslash( $data['items'] ) : '';

		parse_str( wp_unslash( $items ), $items );

		if ( ! empty( $items ) ) {
			$total    = isset( $items['line_total'][ $itemId ] ) ? ( $items['line_total'][ $itemId ] ) : false;
			$subtotal = isset( $items['line_subtotal'][ $itemId ] ) ? ( $items['line_subtotal'][ $itemId ] ) : false;

			return $total !== $subtotal;
		}

		return false;
	}

	protected function isActiveByDefault() {
		return false;
	}

	public function getDescription() {
		return __( 'Tiered pricing for admin-made orders from administrator panel. Please note that this may cause some issue if you use 3rd-party plugins that modify the product price. For example Product Addons', 'tier-pricing-table' );
	}

	public function getSlug() {
		return 'manual-orders';
	}
}
