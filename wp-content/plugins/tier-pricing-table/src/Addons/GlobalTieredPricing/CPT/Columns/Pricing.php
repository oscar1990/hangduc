<?php namespace TierPricingTable\Addons\GlobalTieredPricing\CPT\Columns;

use ArrayIterator;
use Exception;
use TierPricingTable\Addons\GlobalTieredPricing\GlobalPricingRule;
use TierPricingTable\Core\ServiceContainerTrait;

class Pricing {

	use ServiceContainerTrait;

	public function getName() {
		return __( 'Pricing', 'tier-pricing-table' );
	}

	public function render( GlobalPricingRule $rule ) {
		try {
			$rule->validatePricing();

			$pricingType = $rule->getTieredPricingType();
			$rules       = $pricingType === 'percentage' ? $rule->getPercentageTieredPricingRules() : $rule->getFixedTieredPricingRules();
			$minimum     = $rule->getMinimum() ? intval( $rule->getMinimum() ) : 1;

			$regularProductPriceString = __( 'Product regular price', 'tier-pricing-table' );

			if ( $rule->getPricingType() === 'flat' ) {
				if ( $rule->getSalePrice() ) {
					$regularProductPriceString = wc_price( $rule->getSalePrice() );
				} else if ( $rule->getRegularPrice() ) {
					$regularProductPriceString = wc_price( $rule->getRegularPrice() );
				}
			} else if ( $rule->getDiscount() ) {
				$regularProductPriceString = $rule->getDiscount() . '%';
			}

			?>

			<?php if ( $rule->getPricingType() === 'flat' ): ?>
				<?php if ( $rule->getRegularPrice() ): ?>
                    <p><?php echo wp_kses_post( sprintf( __( 'Regular Price: <b>%s</b>', 'tier-pricing-table' ), wc_price( $rule->getRegularPrice() ) ) ) ?></p>
				<?php endif; ?>

				<?php if ( $rule->getSalePrice() ): ?>
                    <p><?php echo wp_kses_post( sprintf( __( 'Sale Price: <b>%s</b>', 'tier-pricing-table' ), wc_price( $rule->getSalePrice() ) ) ) ?></p>
				<?php endif; ?>
			<?php else: ?>
				<?php if ( $rule->getDiscount() ): ?>
                    <p><?php echo wp_kses_post( sprintf( __( 'Discount: <b>%s%%</b>', 'tier-pricing-table' ), $rule->getDiscount() ) ) ?></p>
				<?php endif; ?>
			<?php endif; ?>

			<?php if ( empty( $rules ) ) {
				return;
			} ?>

            <table class="wp-list-table widefat fixed striped table-view-list tpt-global-rule-pricing-table">
                <thead>
                <tr>
                    <th>
                        <b><?php esc_html_e( 'Quantity', 'tier-pricing-table' ); ?></b></th>
                    <th>
                        <b>
							<?php if ( $pricingType === 'percentage' ): ?>
								<?php esc_html_e( 'Discount', 'tier-pricing-table' ); ?>
							<?php else: ?>
								<?php esc_html_e( 'Price', 'tier-pricing-table' ); ?>
							<?php endif; ?>
                        </b>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
						<?php
						if ( 1 >= array_keys( $rules )[0] - $minimum ) : ?>
                            <span><?php echo esc_attr( number_format_i18n( $minimum ) ); ?></span>
						<?php else : ?>
                            <span><?php echo esc_attr( number_format_i18n( $minimum ) ); ?> - <?php echo esc_attr( number_format_i18n( array_keys( $rules )[0] - 1 ) ); ?></span>
						<?php endif; ?>
                    </td>

                    <td>
						<?php if ( $pricingType === 'percentage' ): ?>
							<?php echo wp_kses_post( $regularProductPriceString ); ?>
						<?php else: ?>
							<?php echo wp_kses_post( $regularProductPriceString ); ?>
						<?php endif; ?>
                    </td>
                </tr>

				<?php $iterator = new ArrayIterator( $rules ); ?>

				<?php while ( $iterator->valid() ) : ?>
					<?php
					$currentPrice    = $iterator->current();
					$currentQuantity = $iterator->key();

					$iterator->next();

					if ( $iterator->valid() ) {
						$quantity = $currentQuantity;

						if ( intval( $iterator->key() - 1 != $currentQuantity ) ) {

							$quantity = number_format_i18n( $quantity );

							if ( $this->getContainer()->getSettings()->get( 'quantity_type', 'range' ) === "range" ) {
								$quantity .= ' - ' . number_format_i18n( intval( $iterator->key() - 1 ) );
							}
						}
					} else {
						$quantity = number_format_i18n( $currentQuantity ) . '+';
					}
					?>
                    <tr>
                        <td>
							<?php echo esc_attr( $quantity ); ?>
                        </td>

                        <td>

							<?php if ( $pricingType === 'percentage' ): ?>
								<?php echo $currentPrice . '%' ?>

							<?php else: ?>
								<?php echo wc_price( $currentPrice ); ?>

							<?php endif; ?>

                        </td>
                    </tr>
				<?php endwhile; ?>


                </tbody>
            </table>
			<?php

		} catch ( Exception $e ) {
			echo wp_kses_post( '<div class="help_tip tpt-rule-status tpt-rule-status--invalid" data-tip="' . $e->getMessage() . '">!</div>' );
		}
	}
}
