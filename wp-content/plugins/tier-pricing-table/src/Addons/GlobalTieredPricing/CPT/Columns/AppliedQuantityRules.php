<?php namespace TierPricingTable\Addons\GlobalTieredPricing\CPT\Columns;

use TierPricingTable\Addons\GlobalTieredPricing\GlobalPricingRule;

class AppliedQuantityRules {

	public function getName() {
		return __( 'Minimum Order Quantity', 'tier-pricing-table' );
	}

	public function render( GlobalPricingRule $rule ) {

		$notSetLabel = __( 'Not set', 'tier-pricing-table' );

		$min = $rule->getMinimum() ? $rule->getMinimum() : $notSetLabel;

		// translators: %s: minimum amount
		echo '<b>' . esc_html( $min ) . '</b>';
	}
}
