<?php namespace TierPricingTable\Addons\GlobalTieredPricing\CPT\Columns;

use TierPricingTable\Addons\GlobalTieredPricing\GlobalPricingRule;

class AppliedPricingType {

	public function getName() {
		return __( 'Pricing Applying As', 'tier-pricing-table' );
	}

	public function render( GlobalPricingRule $rule ) {
		echo $rule->getApplyingType() === 'individual' ? __('Individually for products', 'tier-pricing-table') : __('Cross all the products', 'tier-pricing-table');
	}
}
