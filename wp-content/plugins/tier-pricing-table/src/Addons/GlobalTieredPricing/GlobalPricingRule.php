<?php

namespace TierPricingTable\Addons\GlobalTieredPricing;

use  Exception ;
use  WC_Product ;
use  WP_User ;
class GlobalPricingRule
{
    /**
     * Rule ID
     *
     * @var int
     */
    public  $id ;
    /**
     * Is suspended
     *
     * @var bool
     */
    public  $isSuspended ;
    /**
     * Pricing type
     *
     * @var string
     */
    public  $pricingType ;
    /**
     * Regular price
     *
     * @var float
     */
    public  $regularPrice ;
    /**
     * Sale price
     *
     * @var float
     */
    public  $salePrice ;
    /**
     * Sale price
     *
     * @var float
     */
    public  $discount ;
    /**
     * Applying type
     *
     * @var string
     */
    public  $applyingType ;
    /**
     * Tiered Pricing type
     *
     * @var string
     */
    public  $tieredPricingType ;
    /**
     * Percentage Tiered Pricing Rules
     *
     * @var array
     */
    public  $percentageTieredPricingRules ;
    /**
     * Fixed Tiered Pricing Rules
     *
     * @var array
     */
    public  $fixedTieredPricingRules ;
    /**
     * Included categories
     *
     * @var array
     */
    public  $includedProductCategories = array() ;
    /**
     * Included products
     *
     * @var array
     */
    public  $includedProducts = array() ;
    /**
     * Included product roles
     *
     * @var array
     */
    public  $includedUsersRole = array() ;
    /**
     * Included users
     *
     * @var array
     */
    public  $includedUsers = array() ;
    /**
     * Product minimum purchase quantity
     *
     * @var int
     */
    public  $minimum ;
    public function isPurchasable()
    {
        // check this
        return true;
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     */
    public function setId( $id )
    {
        $this->id = $id;
    }
    
    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    
    /**
     * @param float $discount
     */
    public function setDiscount( $discount )
    {
        if ( $discount ) {
            $discount = min( 100, $discount );
        }
        $this->discount = ( $discount ? (double) $discount : null );
    }
    
    /**
     * @return string
     */
    public function getTieredPricingType()
    {
        return 'fixed';
    }
    
    /**
     * @param string $tieredPricingType
     */
    public function setTieredPricingType( $tieredPricingType )
    {
        $this->tieredPricingType = ( in_array( $tieredPricingType, array( 'percentage', 'fixed' ) ) ? $tieredPricingType : 'fixed' );
    }
    
    /**
     * @return array
     */
    public function getPercentageTieredPricingRules()
    {
        return $this->percentageTieredPricingRules;
    }
    
    /**
     * @param array $percentageTieredPricingRules
     */
    public function setPercentageTieredPricingRules( array $percentageTieredPricingRules )
    {
        $this->percentageTieredPricingRules = $percentageTieredPricingRules;
    }
    
    /**
     * @return array
     */
    public function getFixedTieredPricingRules()
    {
        return $this->fixedTieredPricingRules;
    }
    
    /**
     * @param array $fixedTieredPricingRules
     */
    public function setFixedTieredPricingRules( array $fixedTieredPricingRules )
    {
        $this->fixedTieredPricingRules = $fixedTieredPricingRules;
    }
    
    /**
     * @return string
     */
    public function getApplyingType()
    {
        return $this->applyingType;
    }
    
    /**
     * @param string $applyingType
     */
    public function setApplyingType( $applyingType )
    {
        $this->applyingType = ( in_array( $applyingType, array( 'individual', 'cross' ) ) ? $applyingType : 'cross' );
    }
    
    /**
     * Get pricing type
     *
     * @return string
     */
    public function getPricingType()
    {
        return $this->pricingType;
    }
    
    /**
     * Set pricing type
     *
     * @param string $priceType
     */
    public function setPricingType( $priceType )
    {
        $this->pricingType = ( in_array( $priceType, array( 'percentage', 'flat' ) ) ? $priceType : 'flat' );
    }
    
    public function getTieredPricingRules()
    {
        return $this->getFixedTieredPricingRules();
    }
    
    /**
     * Get regular price
     *
     * @return string
     */
    public function getRegularPrice()
    {
        return $this->regularPrice;
    }
    
    /**
     * Set regular price
     *
     * @param string $regularPrice
     */
    public function setRegularPrice( $regularPrice )
    {
        $this->regularPrice = ( $regularPrice ? floatval( $regularPrice ) : null );
    }
    
    /**
     * Get sale price
     *
     * @return string
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }
    
    /**
     * Set sale price
     *
     * @param string $salePrice
     */
    public function setSalePrice( $salePrice )
    {
        $this->salePrice = ( $salePrice ? floatval( $salePrice ) : null );
    }
    
    /**
     * Create instance from array
     *
     * @param array $data
     *
     * @return static
     */
    public static function fromArray( $data )
    {
        $applyingType = ( isset( $data['applying_type'] ) ? $data['applying_type'] : 'individual' );
        $applyingType = ( in_array( $applyingType, array( 'individual', 'cross' ) ) ? $applyingType : 'cross' );
        $tieredPricingType = ( isset( $data['tiered_pricing_type'] ) ? $data['tiered_pricing_type'] : 'fixed' );
        $tieredPricingType = ( in_array( $tieredPricingType, array( 'flat', 'percentage' ) ) ? $tieredPricingType : 'fixed' );
        $percentageRules = ( isset( $data['percentage_rules'] ) ? (array) $data['percentage_rules'] : array() );
        $fixedRules = ( isset( $data['fixed_rules'] ) ? (array) $data['fixed_rules'] : array() );
        $pricingType = ( isset( $data['pricing_type'] ) ? $data['pricing_type'] : 'flat' );
        $pricingType = ( in_array( $pricingType, array( 'flat', 'percentage' ) ) ? $pricingType : 'flat' );
        $regularPrice = ( isset( $data['regular_price'] ) ? (double) $data['regular_price'] : null );
        $salePrice = ( isset( $data['sale_price'] ) ? (double) $data['sale_price'] : false );
        $discount = ( isset( $data['discount'] ) ? (double) $data['discount'] : false );
        $minimum = ( isset( $data['minimum'] ) ? $data['minimum'] : null );
        $self = new static();
        $self->setPricingType( $pricingType );
        $self->setRegularPrice( $regularPrice );
        $self->setSalePrice( $salePrice );
        $self->setDiscount( $discount );
        $self->setApplyingType( $applyingType );
        $self->setTieredPricingType( $tieredPricingType );
        $self->setPercentageTieredPricingRules( $percentageRules );
        $self->setFixedTieredPricingRules( $fixedRules );
        $self->setMinimum( $minimum );
        return $self;
    }
    
    /**
     * Validate
     *
     * @throws Exception
     */
    public function validatePricing()
    {
        $regularPrice = $this->getRegularPrice();
        $salePrice = $this->getSalePrice();
        $tieredPricing = $this->getTieredPricingRules();
        $discount = $this->getDiscount();
        $minimum = $this->getMinimum();
        if ( !$regularPrice && !$salePrice && !$tieredPricing && !$minimum && !$discount ) {
            throw new Exception( __( 'The pricing rule does not affect either prices or minimum quantity. The rule will be skipped.', 'tier-pricing-table' ) );
        }
    }
    
    public function isValidPricing()
    {
        try {
            $this->validatePricing();
        } catch ( Exception $e ) {
            return false;
        }
        return true;
    }
    
    /**
     * Get minimum
     *
     * @return int
     */
    public function getMinimum()
    {
        return $this->minimum;
    }
    
    /**
     * Set minimum
     *
     * @param int $minimum
     */
    public function setMinimum( $minimum )
    {
        $this->minimum = ( $minimum ? intval( $minimum ) : null );
    }
    
    public function getRuleId()
    {
        return $this->id;
    }
    
    /**
     * Get included categories
     *
     * @return array
     */
    public function getIncludedProductCategories()
    {
        return $this->includedProductCategories;
    }
    
    /**
     * Set included categories
     *
     * @param array $includedProductCategories
     */
    public function setIncludedProductCategories( array $includedProductCategories )
    {
        $this->includedProductCategories = $includedProductCategories;
    }
    
    /**
     * Get included products
     *
     * @return array
     */
    public function getIncludedProducts()
    {
        return $this->includedProducts;
    }
    
    /**
     * Set included products
     *
     * @param array $includedProducts
     */
    public function setIncludedProducts( array $includedProducts )
    {
        $this->includedProducts = $includedProducts;
    }
    
    /**
     * Get included user roles
     *
     * @return array
     */
    public function getIncludedUserRoles()
    {
        return $this->includedUsersRole;
    }
    
    /**
     * Set included user roles
     *
     * @param array $includedUsersRole
     */
    public function setIncludedUsersRole( array $includedUsersRole )
    {
        $this->includedUsersRole = $includedUsersRole;
    }
    
    /**
     * Get included users
     *
     * @return array
     */
    public function getIncludedUsers()
    {
        return $this->includedUsers;
    }
    
    /**
     * Set included users
     *
     * @param array $includedUsers
     */
    public function setIncludedUsers( array $includedUsers )
    {
        $this->includedUsers = $includedUsers;
    }
    
    public function asArray()
    {
        return array(
            'pricing_type'        => $this->getPricingType(),
            'regular_price'       => $this->getRegularPrice(),
            'sale_price'          => $this->getSalePrice(),
            'discount'            => $this->getDiscount(),
            'applying_type'       => $this->getApplyingType(),
            'tiered_pricing_type' => $this->getTieredPricingType(),
            'percentage_rules'    => $this->getPercentageTieredPricingRules(),
            'fixed_rules'         => $this->getFixedTieredPricingRules(),
            'minimum'             => $this->getMinimum(),
            'included_categories' => $this->getIncludedProductCategories(),
            'included_products'   => $this->getIncludedProducts(),
            'included_users'      => $this->getIncludedUsers(),
            'included_users_role' => $this->getIncludedUserRoles(),
            'rule_id'             => $this->getRuleId(),
            'is_suspended'        => $this->isSuspended(),
        );
    }
    
    /**
     * Save global price instance
     *
     * @param GlobalPricingRule $rule
     * @param $ruleId
     *
     * @throws Exception
     */
    public static function save( GlobalPricingRule $rule, $ruleId )
    {
        $dataToUpdate = array(
            '_tpt_pricing_type'        => $rule->getPricingType(),
            '_tpt_regular_price'       => $rule->getRegularPrice(),
            '_tpt_sale_price'          => $rule->getSalePrice(),
            '_tpt_discount'            => $rule->getDiscount(),
            '_tpt_applying_type'       => $rule->getApplyingType(),
            '_tpt_tiered_pricing_type' => $rule->getTieredPricingType(),
            '_tpt_percentage_rules'    => $rule->getPercentageTieredPricingRules(),
            '_tpt_fixed_rules'         => $rule->getFixedTieredPricingRules(),
            '_tpt_minimum'             => $rule->getMinimum(),
            '_tpt_included_categories' => $rule->getIncludedProductCategories(),
            '_tpt_included_products'   => $rule->getIncludedProducts(),
            '_tpt_included_users'      => $rule->getIncludedUsers(),
            '_tpt_included_user_roles' => $rule->getIncludedUserRoles(),
            '_tpt_is_suspended'        => ( $rule->isSuspended() ? 'yes' : 'no' ),
        );
        foreach ( $dataToUpdate as $key => $value ) {
            update_post_meta( $ruleId, $key, $value );
        }
    }
    
    public static function build( $ruleId )
    {
        // Simple data to read
        $dataToRead = array(
            '_tpt_pricing_type'        => 'pricing_type',
            '_tpt_sale_price'          => 'sale_price',
            '_tpt_regular_price'       => 'regular_price',
            '_tpt_discount'            => 'discount',
            '_tpt_applying_type'       => 'applying_type',
            '_tpt_tiered_pricing_type' => 'tiered_pricing_type',
            '_tpt_minimum'             => 'minimum',
            '_tpt_is_suspended'        => 'is_suspended',
        );
        $data = array();
        foreach ( $dataToRead as $key => $name ) {
            $data[$name] = get_post_meta( $ruleId, $key, true );
        }
        $priceRule = self::fromArray( $data );
        $existingRoles = wp_roles()->roles;
        $includedCategoriesIds = array_filter( array_map( 'intval', (array) get_post_meta( $ruleId, '_tpt_included_categories', true ) ) );
        $includedProductsIds = array_filter( array_map( 'intval', (array) get_post_meta( $ruleId, '_tpt_included_products', true ) ) );
        $includedUsersRole = array_filter( (array) get_post_meta( $ruleId, '_tpt_included_user_roles', true ), function ( $role ) use( $existingRoles ) {
            return array_key_exists( $role, $existingRoles );
        } );
        $includedUsers = array_filter( array_map( 'intval', (array) get_post_meta( $ruleId, '_tpt_included_users', true ) ) );
        $isSuspended = get_post_meta( $ruleId, '_tpt_is_suspended', true ) === 'yes';
        $priceRule->setPercentageTieredPricingRules( self::readPricingRules( 'percentage', $ruleId ) );
        $priceRule->setFixedTieredPricingRules( self::readPricingRules( 'fixed', $ruleId ) );
        $priceRule->setIncludedProductCategories( $includedCategoriesIds );
        $priceRule->setIncludedUsers( $includedUsers );
        $priceRule->setIncludedUsersRole( $includedUsersRole );
        $priceRule->setIncludedProducts( $includedProductsIds );
        $priceRule->setIsSuspended( $isSuspended );
        $priceRule->setId( $ruleId );
        return $priceRule;
    }
    
    protected static function readPricingRules( $type, $id )
    {
        $type = ( in_array( $type, array( 'percentage', 'fixed' ) ) ? $type : 'fixed' );
        $rules = get_post_meta( $id, "_tpt_{$type}_rules", true );
        $rules = ( !empty($rules) ? $rules : array() );
        ksort( $rules );
        return $rules;
    }
    
    public function setIsSuspended( $isSuspended )
    {
        $this->isSuspended = (bool) $isSuspended;
    }
    
    public function suspend()
    {
        $this->setIsSuspended( true );
    }
    
    public function reactivate()
    {
        $this->setIsSuspended( false );
    }
    
    public function isSuspended()
    {
        return $this->isSuspended;
    }
    
    /**
     * Wrapper for the main "match" function to provide the hook for 3rd party devs
     *
     * @param WP_User $user
     * @param WC_Product $product
     *
     * @return mixed|void
     */
    public function matchRequirements( WP_User $user, WC_Product $product )
    {
        $matched = $this->_matchRequirements( $user, $product );
        return apply_filters(
            'tiered_pricing_table/global_pricing/match_requirements',
            $matched,
            $user,
            $product
        );
    }
    
    protected function _matchRequirements( WP_User $user, WC_Product $product )
    {
        $parentProduct = ( $product->is_type( array( 'variation', 'subscription-variation' ) ) ? wc_get_product( $product->get_parent_id() ) : $product );
        $productMatched = false;
        $productLimitations = false;
        // There are rule limitation for specific products
        
        if ( !empty($this->getIncludedProducts()) ) {
            $productLimitations = true;
            if ( in_array( $product->get_id(), $this->getIncludedProducts() ) || in_array( $parentProduct->get_id(), $this->getIncludedProducts() ) ) {
                $productMatched = true;
            }
        }
        
        
        if ( !empty($this->getIncludedProductCategories()) ) {
            $productLimitations = true;
            if ( !empty(array_intersect( $parentProduct->get_category_ids(), $this->getIncludedProductCategories() )) ) {
                $productMatched = true;
            }
        }
        
        // There is product limitation and the product/category does not match the rule
        if ( $productLimitations && !$productMatched ) {
            return false;
        }
        // Applied to everyone
        if ( empty($this->getIncludedUserRoles()) && empty($this->getIncludedUsers()) ) {
            return true;
        }
        if ( in_array( $user->ID, $this->getIncludedUsers() ) ) {
            return true;
        }
        foreach ( $this->getIncludedUserRoles() as $role ) {
            if ( in_array( $role, $user->roles ) ) {
                return true;
            }
        }
        return false;
    }

}