<?php

namespace TierPricingTable\Addons\GlobalTieredPricing;

use  TierPricingTable\Addons\AbstractAddon ;
use  TierPricingTable\Addons\GlobalTieredPricing\CPT\GlobalTieredPricingCPT ;
use  TierPricingTable\PricingRule ;
use  WC_Product ;
use  WP_User ;
class GlobalTieredPricingAddon extends AbstractAddon
{
    protected  $currentUser = null ;
    /**
     * @var GlobalPricingRule[]
     */
    protected  $globalRules = array() ;
    public function getName()
    {
        return __( 'Global tiered pricing', 'tier-pricing-table' );
    }
    
    public function run()
    {
        new LookupService();
        new GlobalTieredPricingCPT();
        new GlobalTieredPricingCartManager();
        add_action( 'init', function () {
            $this->globalRules = GlobalTieredPricingCPT::getGlobalRules();
        } );
        add_action( 'tiered_pricing_table/admin/pricing_tab_end', array( $this, 'showMessageOnProductsTieredPricingTab' ), 999 );
        /**
         * Main function to filter the tiered pricing rules
         *
         * @priority 30
         */
        add_filter(
            'tiered_pricing_table/price/pricing_rule',
            array( $this, 'addPricing' ),
            30,
            2
        );
    }
    
    public function showMessageOnProductsTieredPricingTab()
    {
        $globalRules = GlobalTieredPricingCPT::getGlobalRules( false );
        if ( empty($globalRules) ) {
            $this->getContainer()->getFileManager()->includeTemplate( 'addons/global-rules/tiered-pricing-tab.php' );
        }
    }
    
    protected function getPrice( WC_Product $product, $specific = false, $originalPrice = false )
    {
        foreach ( $this->globalRules as $pricingRule ) {
            
            if ( $pricingRule->matchRequirements( $this->getCurrentUser(), $product ) ) {
                $pricingType = $pricingRule->getPricingType();
                
                if ( $pricingType === 'percentage' ) {
                    // Do not modify the regular price if discount is set. Adjust the sale price instead.
                    if ( $specific === 'regular' ) {
                        return false;
                    }
                    $discount = $pricingRule->getDiscount();
                    if ( $discount && $originalPrice && $originalPrice > 0 ) {
                        return $originalPrice - $originalPrice / 100 * $discount;
                    }
                } elseif ( $pricingType === 'flat' ) {
                    $salePrice = $pricingRule->getSalePrice();
                    $regularPrice = $pricingRule->getRegularPrice();
                    
                    if ( $specific ) {
                        
                        if ( 'sale' === $specific && $salePrice ) {
                            return $salePrice;
                        } elseif ( 'regular' === $specific && $regularPrice ) {
                            return $regularPrice;
                        }
                    
                    } else {
                        
                        if ( $salePrice ) {
                            return $salePrice;
                        } elseif ( $regularPrice ) {
                            return $regularPrice;
                        }
                    
                    }
                
                }
            
            }
        
        }
        return false;
    }
    
    public function adjustPrice( $originalPrice, WC_Product $product )
    {
        if ( $product->get_meta( 'tiered_pricing_cart_price_calculated' ) === 'yes' ) {
            return $originalPrice;
        }
        $price = $this->getPrice( $product, false, $originalPrice );
        return ( $price ? $price : $originalPrice );
    }
    
    public function adjustSalePrice( $originalSalePrice, WC_Product $product )
    {
        $newPrice = $this->getPrice( $product, 'sale' );
        return ( $newPrice ? (double) $newPrice : $originalSalePrice );
    }
    
    public function adjustRegularPrice( $originalRegularPrice, WC_Product $product )
    {
        $newPrice = $this->getPrice( $product, 'regular' );
        return ( $newPrice ? (double) $newPrice : $originalRegularPrice );
    }
    
    /**
     * Main function to filter tiered pricing rules
     *
     * @param  PricingRule  $pricingRule
     * @param $productId
     *
     * @return PricingRule
     */
    public function addPricing( PricingRule $pricingRule, $productId )
    {
        // Do not modify if there is pricing rules set (in product or role-based or category-based)
        if ( !empty($pricingRule->getRules()) ) {
            return $pricingRule;
        }
        $product = wc_get_product( $productId );
        if ( !$product ) {
            return $pricingRule;
        }
        foreach ( $this->globalRules as $globalPricingRule ) {
            
            if ( !empty($globalPricingRule->getTieredPricingRules()) && $globalPricingRule->matchRequirements( $this->getCurrentUser(), $product ) ) {
                $rules = $globalPricingRule->getTieredPricingRules();
                $rulesType = $globalPricingRule->getTieredPricingType();
                
                if ( !empty($rules) ) {
                    $pricingRule->setRules( $rules );
                    $pricingRule->setType( $rulesType );
                }
                
                $pricingRule->setPricingProvider( 'Global pricing rule - ' . $globalPricingRule->getId() );
                break;
            }
        
        }
        return $pricingRule;
    }
    
    /**
     * @return null
     */
    public function getCurrentUser()
    {
        
        if ( !empty($GLOBALS['tpt_current_user_id']) ) {
            return new WP_User( intval( $GLOBALS['tpt_current_user_id'] ) );
        } else {
            return new WP_User( get_current_user_id() );
        }
    
    }
    
    protected function getCurrentUserRoles()
    {
        $roles = array();
        $user = $this->getCurrentUser();
        if ( $user ) {
            $roles = (array) $user->roles;
        }
        return $roles;
    }
    
    public function getDescription()
    {
        return __( 'Global tiered pricing rules allow you create pricing rules for a user role or particular user and apply them to specific products or a whole product category', 'tier-pricing-table' );
    }
    
    public function getSlug()
    {
        return 'global-tier-pricing';
    }

}