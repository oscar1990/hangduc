<?php namespace TierPricingTable\Addons\GlobalTieredPricing;

use TierPricingTable\Addons\GlobalTieredPricing\CPT\GlobalTieredPricingCPT;

class GlobalTieredPricingCartManager {
	/**
	 * @var GlobalPricingRule[]
	 */
	public $globalPricingRules = array();

	public function __construct() {

		add_action( 'init', function () {
			$this->globalPricingRules = GlobalTieredPricingCPT::getGlobalRules();
		} );

		add_filter( 'woocommerce_add_cart_item_data', array( $this, 'matchPricingRules' ), 10, 3 );

		add_filter( 'tiered_pricing_table/cart/total_product_count', array(
			$this,
			'calculateCommonQuantities'
		), 10, 2 );

		add_action( 'woocommerce_after_cart_item_name', function ( $cartItem ) {

			if ( ! apply_filters( 'tiered_pricing_table/global_pricing/debug_mode', false ) ) {
				return;
			}

			if ( ! current_user_can( 'manage_options' ) ) {
				return;
			}

			if ( ! empty( $cartItem['tpt_pricing_rule_id'] ) ) {
				?>
                <br>
                <span>
                    <b>Applied pricing rule: </b>
                    <a target="_blank"
                       href="<?php echo esc_attr( get_edit_post_link( $cartItem['tpt_pricing_rule_id'] ) ) ?>"><?php echo esc_attr( $cartItem['tpt_pricing_rule_id'] ); ?></a>
                </span>
				<?php
			}
		} );
	}

	public function matchPricingRules( $cartItem, $productId, $variationId ) {

		$user      = new \WP_User( get_current_user_id() );
		$productId = $variationId ? $variationId : $productId;
		$product   = wc_get_product( $productId );

		if ( ! $product ) {
			return $cartItem;
		}

		foreach ( $this->globalPricingRules as $globalRule ) {
			if ( $globalRule->matchRequirements( $user, $product ) && $globalRule->getApplyingType() === 'cross' ) {
				$cartItem['tpt_pricing_rule_id'] = $globalRule->getId();
				break;
			}
		}

		return $cartItem;
	}

	public function calculateCommonQuantities( $quantity, $cartItem ) {

		if ( empty( $cartItem['tpt_pricing_rule_id'] ) ) {
			return $quantity;
		}

		$pricingRuleId = $cartItem['tpt_pricing_rule_id'];

		$quantity = 0;

		foreach ( wc()->cart->get_cart_contents() as $cartItem ) {
			if ( ! empty( $cartItem['tpt_pricing_rule_id'] ) && $cartItem['tpt_pricing_rule_id'] === $pricingRuleId ) {
				$quantity += $cartItem['quantity'];
			}
		}

		return $quantity;
	}
}