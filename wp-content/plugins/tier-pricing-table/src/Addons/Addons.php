<?php

namespace TierPricingTable\Addons;

use  TierPricingTable\Addons\CategoryTiers\CategoryTierAddon ;
use  TierPricingTable\Addons\GlobalTieredPricing\GlobalTieredPricingAddon ;
use  TierPricingTable\Addons\ManualOrders\ManualOrdersAddon ;
use  TierPricingTable\Addons\MinQuantity\MinQuantity ;
use  TierPricingTable\Addons\RoleBasedPricing\RoleBasedPricingAddon ;
use  TierPricingTable\Addons\Coupons\Coupons ;
use  TierPricingTable\Core\ServiceContainerTrait ;
class Addons
{
    use  ServiceContainerTrait ;
    /**
     * Addons constructor.
     */
    public function __construct()
    {
        $this->init();
    }
    
    public function init()
    {
        $addons = array(
            'CategoryTiers'       => new CategoryTierAddon(),
            'ManualOrders'        => new ManualOrdersAddon(),
            'RoleBasedPricing'    => new RoleBasedPricingAddon(),
            'GlobalTieredPricing' => new GlobalTieredPricingAddon(),
            'Coupons'             => new Coupons(),
        );
        $addons = apply_filters( 'tiered_pricing_table/addons/list', $addons );
        foreach ( $addons as $key => $addon ) {
            if ( $addon->isEnabled() ) {
                $addon->run();
            }
        }
    }

}