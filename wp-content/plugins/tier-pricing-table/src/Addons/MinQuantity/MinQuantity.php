<?php namespace TierPricingTable\Addons\MinQuantity;

use TierPricingTable\Addons\AbstractAddon;
use TierPricingTable\PriceManager;
use TierPricingTable\TierPricingTablePlugin;
use WC_Cart;
use WC_Product;

class MinQuantity extends AbstractAddon {

	public function getName() {
		return __( 'Minimum quantity validation', 'tier-pricing-table' );
	}

	public function run() {

		add_action( 'woocommerce_before_calculate_totals', function ( WC_Cart $cart ) {
			foreach ( $cart->get_cart_contents() as $cartItemKey => $cartItem ) {
				if ( $cartItem['data'] instanceof WC_Product ) {
					$productId = ! empty( $cartItem['variation_id'] ) ? $cartItem['variation_id'] : $cartItem['product_id'];

					$pricingRule = PriceManager::getPricingRule( $productId );
					$min         = $pricingRule->getMinimum();

					if ( $this->getProductCartQuantity( $cartItem['product_id'], 'product', $cart ) < $min ) {
						$cart->remove_cart_item( $cartItemKey );
						wc_add_notice( sprintf( __( 'Minimum quantity for the %s is %d', 'tier-pricing-table' ),
							$cartItem['data']->get_name(), $min ), 'error' );
						continue;
					}
				}
			}
		} );

		add_filter( 'woocommerce_quantity_input_args', function ( $args ) {
			global $product;

			if ( $product && $product instanceof WC_Product && TierPricingTablePlugin::isSimpleProductSupported( $product ) ) {
				$pricingRule       = PriceManager::getPricingRule( $product->get_id() );
				$min               = $pricingRule->getMinimum();
				$min               = max( 1, $min - $this->getProductCartQuantity( $product->get_id() ) );
				$args['min_value'] = $min;
			}

			return $args;
		} );

		add_filter( 'woocommerce_add_to_cart_validation', function ( $passed, $productId, $qty ) {
			$pricingRule = PriceManager::getPricingRule( $productId );
			$min         = $pricingRule->getMinimum();
			$min         = max( 1, $min - $this->getProductCartQuantity( $productId ) );

			if ( $qty < $min ) {

				wc_add_notice( sprintf( __( 'Minimum quantity for the product is %s', 'tier-pricing-table' ), $min ),
					'error' );

				return false;
			}

			return $passed;

		}, 10, 3 );

		add_filter( 'woocommerce_update_cart_validation', function ( $passed, $cart_item_key, $values, $quantity ) {
			$productId   = $values['variation_id'] ? $values['variation_id'] : $values['product_id'];
			$pricingRule = PriceManager::getPricingRule( $productId );
			$min         = $pricingRule->getMinimum();
			$min         = max( 1, $min - $this->getProductCartQuantity( $values['product_id'] ) );

			if ( $quantity < $min ) {
				wc_add_notice( sprintf( __( 'Minimum quantity for the product is %s', 'tier-pricing-table' ), $min ),
					'error' );

				return false;
			}

			return $passed;

		}, 10, 4 );

		add_filter( 'woocommerce_available_variation', function ( $variation ) {
			$pricingRule = PriceManager::getPricingRule( $variation['variation_id'] );
			$min         = $pricingRule->getMinimum();

			$min = max( 1, $min - $this->getProductCartQuantity( $variation['variation_id'], 'variation' ) );

			$variation['min_qty']   = $min;
			$variation['qty_value'] = $min;

			return $variation;
		} );

		add_action( 'wp_head', function () {

			if ( ! is_product() ) {
				return;
			}

			?>
			<script>
                /**
                 * Handle Minimum Quantities by Tiered Pricing Table
                 */
                (function ($) {

                    $(document).on('found_variation', function (event, variation) {
                        if (typeof variation.qty_value !== "undefined") {
                            // update quantity field with a new minimum
                            $('form.cart').find('[name=quantity]').val(variation.qty_value)
                        }

                        if (typeof variation.min_qty !== "undefined") {
                            // update quantity field with a new minimum
                            $('form.cart').find('[name=quantity]').attr('min', variation.min_qty);
                        }
                    });

                })(jQuery);

			</script>
			<?php
		} );

	}

	protected function getProductCartQuantity( $productId, $type = 'product', $cart = null ) {
		$qty  = 0;
		$cart = $cart ? $cart : wc()->cart;

		if ( is_array( $cart->cart_contents ) ) {
			foreach ( $cart->cart_contents as $cartItem ) {

				if ( $type === 'variation' ) {
					$compare = ! empty( $cartItem['variation_id'] ) ? $cartItem['variation_id'] : 0;
				} else {
					$compare = $cartItem['product_id'];
				}

				if ( $compare == $productId ) {
					$qty += $cartItem['quantity'];
				}
			}
		}

		return apply_filters( 'tiered_pricing_table/minimum_quantity/item_quantity', $qty, $productId );
	}

	public function getDescription() {
		return __( 'Minimum quantity validation. Turning off might be useful in case you use a 3rd-party plugin for MOQ',
			'tier-pricing-table' );
	}

	public function getSlug() {
		return 'minimum-quantity';
	}
}
