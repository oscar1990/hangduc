<?php

namespace TierPricingTable\Admin;

use  TierPricingTable\Addons\GlobalTieredPricing\CPT\GlobalTieredPricingCPT ;
use  TierPricingTable\Admin\ProductManagers\ProductManager ;
use  TierPricingTable\Core\ServiceContainerTrait ;
use  TierPricingTable\Settings\Settings ;
use  TierPricingTable\TierPricingTablePlugin ;
use  TierPricingTable\Admin\ProductManagers\SimpleProductManager ;
use  TierPricingTable\Admin\ProductManagers\VariationProductManager ;
use  TierPricingTable\Admin\Export\Woocommerce as WooCommerceExport ;
use  TierPricingTable\Admin\Import\Woocommerce as WooCommerceImport ;
use  TierPricingTable\Admin\Import\WPAllImport ;
/**
 * Class Admin
 *
 * @package TierPricingTable\Admin
 */
class Admin
{
    use  ServiceContainerTrait ;
    /**
     * Array of Managers
     *
     * @var array
     */
    private  $managers ;
    /**
     * Admin constructor.
     *
     * Register menu items and handlers
     *
     */
    public function __construct()
    {
        new ProductManager();
        new VariationProductManager();
        new WooCommerceExport();
        new WooCommerceImport();
        add_action(
            'admin_enqueue_scripts',
            [ $this, 'enqueueAssets' ],
            10,
            2
        );
        if ( get_transient( 'tiered_pricing_table_activated' ) === 'yes' ) {
            add_action( 'admin_notices', [ $this, 'showActivationMessage' ] );
        }
        
        if ( !tpt_fs()->is_premium() ) {
            $template = 'upgrade-alert.php';
            add_action( 'woocommerce_settings_' . Settings::SETTINGS_PAGE, function () use( $template ) {
                $this->getContainer()->getFileManager()->includeTemplate( 'admin/alerts/' . $template, [
                    'upgradeUrl'   => tpt_fs_activation_url(),
                    'contactUsUrl' => TierPricingTablePlugin::getContactUsURL(),
                ] );
            } );
        }
    
    }
    
    /**
     * Show message about activation plugin and advise next step
     */
    public function showActivationMessage()
    {
        $link = $this->getContainer()->getSettings()->getLink();
        $this->getContainer()->getFileManager()->includeTemplate( 'admin/alerts/activation-alert.php', array(
            'link'             => $link,
            'documentationURL' => TierPricingTablePlugin::getDocumentationURL(),
            'settingsURL'      => $this->getContainer()->getSettings()->getLink(),
        ) );
        delete_transient( 'tiered_pricing_table_activated' );
    }
    
    /**
     * Register assets on product create/update page
     *
     * @param $page
     */
    public function enqueueAssets( $page )
    {
        wp_enqueue_script(
            'tiered-pricing-table-admin-js',
            $this->getContainer()->getFileManager()->locateJSAsset( 'admin/main' ),
            [ 'jquery' ],
            TierPricingTablePlugin::VERSION
        );
        wp_enqueue_style(
            'tiered-pricing-table-admin-css',
            $this->getContainer()->getFileManager()->locateAsset( 'admin/style.css' ),
            array(),
            TierPricingTablePlugin::VERSION
        );
    }

}