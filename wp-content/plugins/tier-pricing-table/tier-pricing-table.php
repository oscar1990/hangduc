<?php

use TierPricingTable\TierPricingTablePlugin;

/**
 * Plugin Name: WooCommerce Tiered Price Table (Premium)
 * Description:       Quantity-based discounts with nice-looking reflection on the product page.
 * Version:           3.5.1
 * Author:            bycrik
 * Author URI:        https://u2code.com
 * Plugin URI:        https://u2code.com/plugins/tiered-pricing-table-for-woocommerce/
 * License:           GPLv2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tier-pricing-table
 * Domain Path:       /languages/
 *
 * WC requires at least: 4.0
 * WC tested up to: 8.0
 *
  */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( function_exists( 'tpt_fs' ) ) {
	tpt_fs()->set_basename( false, __FILE__ );
} else {

	if ( ! function_exists( 'tpt_fs' ) ) {
		require_once 'licence-init.php';
	}

	call_user_func( function () {

		require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

		$main = new TierPricingTablePlugin( __FILE__ );

		register_activation_hook( __FILE__, array( $main, 'activate' ) );

		add_action( 'uninstall', array( TierPricingTablePlugin::class, 'uninstall' ) );

		$main->run();
	} );
}

define('TIERED_PRICING_PRODUCTION', true);
