<?php

use  TierPricingTable\PriceManager ;
use  TierPricingTable\Settings\Sections\GeneralSection\GeneralSection ;
use  TierPricingTable\Settings\Settings ;
if ( !defined( 'WPINC' ) ) {
    die;
}
/**
 * Available variables
 *
 * @var array $price_rules
 * @var string $real_price
 * @var string $product_name
 * @var string $pricing_type
 * @var WC_Product $product
 * @var string $id
 * @var int $product_id
 * @var int $minimum
 * @var array $settings
 */
$sale_price = $product->get_sale_price();
if ( $sale_price ) {
    $sale_price = wc_get_price_to_display( $product, array(
        'price' => $sale_price,
    ) );
}
$regular_price = wc_get_price_to_display( $product, array(
    'price' => $product->get_regular_price(),
) );
$price = wc_get_price_to_display( $product, array(
    'price' => $product->get_price(),
) );
if ( !function_exists( 'tptParseOptionText' ) ) {
    function tptParseOptionText( $text, $quantity, $discount = null )
    {
        return strtr( $text, array(
            '{tp_quantity}'         => $quantity,
            '{tp_discount}'         => $discount,
            '{tp_rounded_discount}' => round( $discount ),
        ) );
    }

}

if ( !empty($price_rules) ) {
    ?>

	<div class="tiered-pricing-wrapper">
		<?php 
    
    if ( !empty($settings['title']) ) {
        ?>
			<h3 style="clear:both; margin: 20px 0;"><?php 
        echo  esc_attr( $settings['title'] ) ;
        ?></h3>
		<?php 
    }
    
    ?>

		<div class="tiered-pricing-options"
		     id="<?php 
    echo  esc_attr( $id ) ;
    ?>"
		     data-product-id="<?php 
    echo  esc_attr( $product_id ) ;
    ?>"
		     data-price-rules="<?php 
    echo  esc_attr( htmlspecialchars( json_encode( $price_rules ) ) ) ;
    ?>"
		     data-minimum="<?php 
    echo  esc_attr( $minimum ) ;
    ?>"
		     data-product-name="<?php 
    echo  esc_attr( $product_name ) ;
    ?>"
		     data-regular-price="<?php 
    echo  esc_attr( $regular_price ) ;
    ?>"
		     data-sale-price="<?php 
    echo  esc_attr( $sale_price ) ;
    ?>"
		     data-price="<?php 
    echo  esc_attr( $price ) ;
    ?>"
		     data-product-price-suffix="<?php 
    echo  esc_attr( $product->get_price_suffix() ) ;
    ?>"
		>
			<div class="tiered-pricing-option tiered-pricing--active tiered-pricing-option--default"
			     data-tiered-quantity="<?php 
    echo  esc_attr( $minimum ) ;
    ?>"
			     data-tiered-price="
				<?php 
    echo  esc_attr( wc_get_price_to_display( wc_get_product( $product_id ), array(
        'price' => $real_price,
    ) ) ) ;
    ?>
				"
			     data-tiered-price-exclude-taxes="
				<?php 
    echo  esc_attr( wc_get_price_excluding_tax( wc_get_product( $product_id ), array(
        'price' => $real_price,
    ) ) ) ;
    ?>"
			     data-tiered-price-include-taxes="
				<?php 
    echo  esc_attr( wc_get_price_including_tax( wc_get_product( $product_id ), array(
        'price' => $real_price,
    ) ) ) ;
    ?>"
			>
				<div class="tiered-pricing-option__checkbox">
					<div class="tiered-pricing-option-checkbox tiered-pricing-option-checkbox--active"></div>
				</div>

				<div class="tiered-pricing-option__quantity">
					<?php 
    
    if ( 1 >= array_keys( $price_rules )[0] - $minimum || $settings['quantity_type'] === "static" ) {
        ?>
						<?php 
        $quantity = esc_attr( number_format_i18n( $minimum ) . ' ' );
        ?>
					<?php 
    } else {
        ?>
						<?php 
        $quantity = esc_attr( number_format_i18n( $minimum ) . ' - ' . number_format_i18n( array_keys( $price_rules )[0] - 1 ) . ' ' );
        ?>
					<?php 
    }
    
    ?>

					<?php 
    echo  tptParseOptionText( GeneralSection::getDefaultOptionText(), $quantity ) ;
    ?>
				</div>

				<div class="tiered-pricing-option__pricing">
					<div class="tiered-pricing-option-price">
						<div class="tiered-pricing-option-price__discounted">
							<?php 
    echo  wp_kses_post( wc_price( wc_get_price_to_display( wc_get_product( $product_id ), array(
        'price' => $real_price,
    ) ) ) ) ;
    ?>
						</div>
					</div>
					<div class="tiered-pricing-option-total">
						<div class="tiered-pricing-option-total__label">
							<?php 
    esc_html_e( 'Total:', 'tier-pricing-table' );
    ?>
						</div>
						<div class="tiered-pricing-option-total__discounted_total">
							<?php 
    echo  wp_kses_post( wc_price( wc_get_price_to_display( wc_get_product( $product_id ), array(
        'price' => $real_price,
    ) ) ) ) ;
    ?>
						</div>
					</div>
				</div>
			</div>

			<?php 
    $iterator = new ArrayIterator( $price_rules );
    ?>

			<?php 
    while ( $iterator->valid() ) {
        ?>
				<?php 
        $currentPrice = $iterator->current();
        $currentQuantity = $iterator->key();
        
        if ( $pricing_type === 'percentage' ) {
            $discountAmount = $currentPrice;
        } else {
            $discountAmount = PriceManager::calculateDiscount( $real_price, $currentPrice );
        }
        
        $iterator->next();
        
        if ( $iterator->valid() ) {
            $quantity = $currentQuantity;
            
            if ( intval( $iterator->key() - 1 != $currentQuantity ) ) {
                $quantity = number_format_i18n( $quantity );
                if ( $settings['quantity_type'] === "range" ) {
                    $quantity .= ' - ' . number_format_i18n( intval( $iterator->key() - 1 ) );
                }
            }
        
        } else {
            $quantity = sprintf( '%s+', number_format_i18n( $currentQuantity ) );
        }
        
        $currentProductPrice = PriceManager::getPriceByRules( $currentQuantity, $product_id );
        $currentProductPriceExcludeTaxes = wc_get_price_excluding_tax( wc_get_product( $product_id ), array(
            'price' => PriceManager::getPriceByRules(
            $currentQuantity,
            $product_id,
            null,
            null,
            false
        ),
        ) );
        $currentProductPriceIncludeTaxes = wc_get_price_including_tax( wc_get_product( $product_id ), array(
            'price' => PriceManager::getPriceByRules(
            $currentQuantity,
            $product_id,
            null,
            null,
            false
        ),
        ) );
        ?>

				<div class="tiered-pricing-option"
				     data-tiered-quantity="<?php 
        echo  esc_attr( $currentQuantity ) ;
        ?>"
				     data-tiered-price="<?php 
        echo  esc_attr( $currentProductPrice ) ;
        ?>"
				     data-tiered-price-exclude-taxes="<?php 
        echo  esc_attr( $currentProductPriceExcludeTaxes ) ;
        ?>"
				     data-tiered-price-include-taxes="<?php 
        echo  esc_attr( $currentProductPriceIncludeTaxes ) ;
        ?>">

					<div class="tiered-pricing-option__checkbox">
						<div class="tiered-pricing-option-checkbox"></div>
					</div>
					<div class="tiered-pricing-option__quantity">
						<?php 
        echo  tptParseOptionText( GeneralSection::getOptionText(), $quantity, round( $discountAmount, 2 ) ) ;
        ?>
					</div>
					<div class="tiered-pricing-option__pricing">
						<div class="tiered-pricing-option-price">
							<div class="tiered-pricing-option-price__original">
								<del>
									<?php 
        echo  wp_kses_post( wc_price( wc_get_price_to_display( $product, array(
            'price' => $real_price,
        ) ) ) ) ;
        ?>
								</del>
							</div>
							<div class="tiered-pricing-option-price__discounted">
								<?php 
        echo  wp_kses_post( wc_price( PriceManager::getPriceByRules( $currentQuantity, $product_id ) ) ) ;
        ?>
							</div>
						</div>
						<div class="tiered-pricing-option-total">
							<div class="tiered-pricing-option-total__label">
								<?php 
        esc_html_e( 'Total:', 'tier-pricing-table' );
        ?>
							</div>
							<div class="tiered-pricing-option-total__original_total">

							</div>
							<div class="tiered-pricing-option-total__discounted_total">

							</div>
						</div>
					</div>
				</div>
			<?php 
    }
    ?>
		</div>

	</div>

	<style>
		<?php 
    if ( !$settings['options_show_default_option'] ) {
        echo  esc_html( "#{$id} .tiered-pricing-option--default { display: none }" ) ;
    }
    $backgroundColor = Settings::hex2rgba( $settings['active_tier_color'], 0.05 );
    echo  esc_html( "#{$id} .tiered-pricing--active .tiered-pricing-option-checkbox::after {\n\t\t\tbackground: {$settings['active_tier_color']};\n\t\t}" ) ;
    echo  esc_html( "#{$id} .tiered-pricing--active .tiered-pricing-option-checkbox {\n\t\t\tborder-color:  {$settings['active_tier_color']};\n\t\t}" ) ;
    echo  esc_html( "#{$id} .tiered-pricing--active {\n\t\t\tborder-color: {$settings['active_tier_color']};\n\t\t\tbackground: {$backgroundColor};\n\t\t}" ) ;
    
    if ( !$settings['options_show_original_product_price'] ) {
        echo  esc_html( "#{$id} .tiered-pricing-option-price__original {\n\t\t\t\tdisplay: none\n\t\t\t}" ) ;
        echo  esc_html( "#{$id} .tiered-pricing-option-total__original_total {\n\t\t\t\tdisplay: none\n\t\t\t}" ) ;
    }
    
    ?>

	</style>
<?php 
}
