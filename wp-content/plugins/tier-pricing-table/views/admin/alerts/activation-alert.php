<?php if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Activation plugin message
 *
 * @var string $link
 * @var string $settingsURL
 * @var string $documentationURL
 */
?>

<div id="message" class="updated notice is-dismissible">
    <p>
        <span style="font-size: 1.3em"><?php esc_html_e( 'Thanks for installing the Tiered Pricing Table. We hope you will enjoy using it!', 'tier-pricing-table' ) ?></span>
    </p>
    <p>
        <a href="<?php echo esc_attr( $settingsURL ) ?>"
           class="button button-primary"><?php esc_html_e( 'Settings', 'tier-pricing-table' ); ?></a>
        <a href="<?php echo esc_attr( $documentationURL ) ?>" target="_blank"
           class="button"><?php esc_html_e( 'Documentation', 'tier-pricing-table' ); ?></a>
    </p>
</div>
