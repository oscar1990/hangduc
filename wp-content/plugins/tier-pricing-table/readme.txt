=== Tiered Pricing Table for WooCommerce ===

Contributors: bycrik, freemius
Tags: woocommerce, tiered pricing, dynamic price, price, wholesale, volume pricing, quantity-based pricing
Requires at least: 4.2
Tested up to: 6.2
Requires PHP: 7.0
Stable tag: 3.5.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Allows you to set a specific price for a certain quantity of product. Shows quantity pricing table/blocks/options/tooltip.

== Description ==

The simple in using a dynamic pricing plugin for WooCommerce allows you to create flexible pricing rules. Quantity-based, Role-based, Category based, and many more.

**Quantity-based pricing** lets you set different prices for different quantities of a product.

**Role-based functionality** lets you have different pricing for different user roles or specific customers including quantity-based pricing.

**Minimum order quantity** lets you allow to set minimum pieces to purchase per product.

The easiest to set up and most powerful wholesale plugin for WooCommerce.
Clean interface, keen functionality, and extendable source code.

Set up flexible pricing rules (quantity-based pricing, role-based pricing, minimum order quantity), display pricing policy (a pricing table, pricing blocks, or pricing options) on the product page, and drive more sales by offering a discount for bulk!

Features:

*   Set specific prices for a certain quantity of product/product variation
*   Create pricing rules to provide discounts for a product category or set of products and limit the rule for specific roles or specific customers
*   Display pricing table / pricing blocks / pricing options / pricing dropdown / tooltip on the product page (supports different places)
*   Customization (titles, colors, positions, and many others)
*   Import/Export (WPAllImport supported)
*   REST API
*   Many other little useful features

Premium features:

*   Percentage discounts for quantity-based rules
*   MOQ (minimum order quantity) per product or category
*   Role-based pricing (including role-specific tiered pricing)
*   Show totals on the product page
*   Set discounts in bulk for product categories
*   Clickable pricing options / table rows / pricing blocks
*   Show the lowest price or range of prices in the products catalog
*   Show the tiered price in the cart as a discount with the original price crossed out
*   Show cart item upsells (motivates users to purchase more to get a discount)
*   Show the total price on the product page instead of the regular price
*   Built-in cache for the pricing strings to provide the best performance

Integration with 3rd-party plugins:

*   WP All Import
*   Elementor
*   WPML
*   Product Bundles for WooCommerce
*   WooCommerce Product Add-ons
*   WOOCS (WooCommerce Currency Switcher by FOX)
*   WCCS (WooCommerce Currency Switcher by WP Experts)
*   Aelia Multicurrency
*   WCPA (WooCommerce Custom Product Addons)
*   WooCommerce Deposits
*   Mix&Match for WooCommerce

### Check out our site to get more information about the [Tiered Pricing Table](https://u2code.com/plugins/tiered-pricing-table-for-woocommerce/)

== Screenshots ==

1. Pricing table on product page
2. Pricing blocks on the product page
3. Pricing options on the product page
4. Totals on the product page
5. Tiered Pricing for simple products
6. Tiered Pricing for variable products
7. Role-based tiered pricing \ regular and sale prices
8. Cross products pricing rules
9. Cart and checkout
10. Catalog prices
11. Integrations
12. Side features

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/tier-price-table` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the WooCommerce->Settings Name screen to configure the plugin

After installing the plugin set up your own settings

== Frequently Asked Questions ==

= How the import format looks like? =

"quantity:price,quantity:price"

For example:
"10:20,20:18" - in this case 20$ at 10 pieces, $18 at 20 pieces or more.
The same format is for the percentage-based rules:
"quantity:discount,quantity:discount"

Please note that you have to use a dot as a decimal separator because a comma is used to separate the pricing rules.

You can change the rules separator (in case you use a comma as a decimal separator) using the "tiered_pricing_table/rules_separator" hook.

= Can I offer discounts across a variable product? =

Yes, you can.
The "summarize all variation" option in the plugin's settings will consider all variations as one product and calculate a discount based on every variation in the cart.

= Can I set up discounts for a product category, or even for every product in my store =

Yes, you can.
You can create a global pricing rule (WooCommerce → Pricing rules) and configure it for product categories, user roles, etc.

= Can I show the pricing table/pricing blocks/pricing options using a shortcode =

Yes, you can. There is the [tiered-pricing-table] available with a bunch of settings.

= Can I show the pricing table/pricing blocks/pricing options via Elementor =

Yes, you can. Look for the "Tiered Pricing Table" widget.

= Can I apply tiered pricing for manual (admin-made) orders? =

Yes, you can. There is an option in the "advanced" section of the setting to enable tiered pricing for admin-made orders

== Changelog ==

= 3.5.1 =
* Fix: Clickable pricing for variable products
* Fix: Pull right pricing when variation is specified in URL
* Fix: CSS for dropdown

= 3.5.0 =
* New: New type of displaying - dropdown
* Fix: Issue when regular prices is replaces by 1$
* Fix: Upsell {tp_actual_discount} variable

= 3.4.3 =
* New: Integration with WCCS
* Fix: Coupons potential error
* Fix: Displaying price with taxes on product page

= 3.4.2 =
* New: HPOS support
* Fix: Minimum order quantity issue for user roles
* Fix: Rounding price hook

= 3.4.1 =
* Fix: Fix default variations

= 3.4.0 =
* New: Cache: performance increased for large variable products
* New: Advanced settings for products: select default variation, mark products that does not use tiered pricing.
* New: Quantity measurement fields in the settings
* Fix: Fix role based rules for manual orders
* Fix: Fix taxes for manual orders

= 3.3.5 =
* New: Freemius SDK updated to 2.5.5
* New: Support "woocommerce_price_trim_zeros" hook
* New: Support role-based rules for manual orders
* New: New hook to override the rules separator during the import
* Fix: WCPA integration

= 3.3.4 =
* Fix: critical MOQ issue with variable products

= 3.3.3 =
* Fix: Legacy hooks infinity loop
* Fix: MOQ custom add to cart handlers
* New: Extended WPML config
* New: New hook for formatting variation prices

= 3.3.2 =
* Fix: Show tiered pricing via shortcode/elementor widget even if the global display option is disabled.
* Fix: Saving percentage tiered pricing rules for variation
* New: Show parent category for selected category
* New: Added more legacy hooks
* New: Make MOQ validation string translatable

= 3.3.1 =
* Fix: Tooltip layout
* Fix: Discount calculations on tiered pricing layouts
* Fix: Do not run frontend script on product that does not have tiered pricing
* New: Legacy hooks

= 3.3.0 =
* New: Supports {price_excluding_tax} and {price_including_tax} price suffix variables
* New: Showing discounted total price with original total crossed out
* New: Cache for price manager
* New: Trial button
* Fix: move to tiered_pricing_table/price/pricing_rule hook

= 3.2.0 =
* New: Cart upsell
* Fix: CSS issues
* Fix: typos

= 3.1.1 =
* New: Notice with global rules on tiered pricing tab
* Fix: issue with global pricing rules
* Fix: price without taxes issue
* Fix: typos

= 3.1.0 =
* New: new way to display the tiered pricing - options
* New: tiered pricing template can be selected per product
* New: little enhancements
* Fix: Firefox JS issue
* Fix: hidden "quick-edit" for products

= 3.0.1 =
* Fix: Default variation table
* Fix: Manual orders are active by default (unable to change order total for admin-made orders)

= 3.0.0 =
* New: Refactoring the plugin structure
* New: Refactoring the frontend script
* New: Global Tiered Pricing rules
* New: Tiered Pricing Blocks
* New: Elementor integration
* New: Settings redesign (added sections, many new settings, refactoring settings script)
* New: Discount column for fixed rules
* New: Tiered Pricing shortcode
* New: Tiered Pricing coupons management
* New: WOOCS integration
* Fix: Double pricing suffix on simple products
* Fix: Minor bugs

= 2.8.2 =
* Fix: Premium upgrading
* Fix: WCPA Integration

= 2.8.1 =
* New: Aelia Multicurrency Integration
* New: WCPA Integration
* New: WooCommerce Bundles Integration
* New: Role-based rules for API
* New: support role-based rules in WooCommerce Import
* New: New Hooks
* Fix: Catalog prices
* Bugs fixes & minor improvements

= 2.8.0 =
* New: REST API
* New: WordPress 6.0 support
* New: WooCommerce 6.6 support
* Bugs fixes & minor improvements

= 2.7.0 =
* New: static quantities for the pricing table
* New: Pricing cache for variable products
* New: WP All Import: "tiered pricing" import option
* Fix: Bugs fixes & minor improvements

= 2.6.1 =
* Security fix
* Fix: WooCommerce Subscription variable products support
* Minor improvements

= 2.6.0 =
* Fix: Minor bugs
* WPML extended support

= 2.5.0 =
* Freemius update
* Bugs fixes
* Performance improvements
* Improved role-based pricing
* WPML support

= 2.4.1 =
* Freemius update
* Bugs fixes
* Minor improvements

= 2.4.0 =
* Role-based pricing for the premium version
* Bug fixes
* Minor improves

= 2.3.7 =
* Addon fixes
* Price Suffix fix
* Minor improves

= 2.3.6 =
* WooCommerce 4 variations fix

= 2.3.5 =
* Fix issues
* Category tiers in premium version

= 2.3.4 =
* Fix ajax issues
* Fix assets issues

= 2.3.3 =
* Fix taxes issue
* Added ability to calculate the tiered price based on all variations
* Added ability to set bulk rules for variable product
* Added support minimum quantity in the PREMIUM version
* Added summary table in PREMIUM version
* minor fixes
* Fixes for the popular themes

= 2.3.2 =
* Fix upgrading

= 2.3.1 =
* Fix the jQuery issue

= 2.3.0 =
* Fix critical bug

= 2.2.3 =
* Fixed bugs
* Added hooks

= 2.2.1 =
* Fixed bugs
* Added total price feature

= 2.2.0 =
* Added Import\Export tiered pricing
* Clickable quantity rows (Premium)
* Fix with some themes
* Fix the mini-cart issue

= 2.1.2 =
* Fixes
* Trial mode

= 2.1.1 =
* Fixes
* Premium variable catalog prices

= 2.1.0 =
* Support WooCommerce Taxes
* Do not show the table head if column texts are blank
* Fix Updater
* Fix little issues

= 2.0.2 =
* Fix JS calculation prices
* Remove the table from variation tier tables

= 2.0.0 =
* Fix bugs
* JS updating prices on the product page
* Tooltip border
* Premium version

= 1.1.0 =
* Fix bug with comma as thousand separators.
* Minor updates