<?php return array(
    'root' => array(
        'name' => 'mynewvk/tier-pricing-table',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'fd8a9d0135cfc02cb423fd9ffb3343dcc30a0452',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'mynewvk/tier-pricing-table' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'fd8a9d0135cfc02cb423fd9ffb3343dcc30a0452',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
